/*
 * This library is part of OpenCms -
 * the Open Source Content Management System
 *
 * Copyright (c) Alkacon Software GmbH (http://www.alkacon.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about Alkacon Software GmbH, please see the
 * company website: http://www.alkacon.com
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.opencms.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Extraced from Opencms 9.0.1
 * <p>
 * Utilities to get and set formated dates in OpenCms.
 * <p>
 * 
 * @since 6.0.0
 */
public final class CmsDateUtil {

	/** The "GMT" time zone, used when formatting http headers. */
	protected static final TimeZone GMT_TIMEZONE = TimeZone.getTimeZone("GMT");

	/** The default format to use when formatting http headers. */
	protected static final DateFormat HEADER_DEFAULT = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);

	/**
	 * Returns a formated date and time String form a timestamp value based on the HTTP-Header date format.
	 * <p>
	 * 
	 * @param time
	 *            the time value to format as date
	 * @return the formatted date
	 */
	public static String getHeaderDate(long time) {

		if (HEADER_DEFAULT.getTimeZone() != GMT_TIMEZONE) {
			// ensure GMT is used as time zone for the header generation
			HEADER_DEFAULT.setTimeZone(GMT_TIMEZONE);
		}

		return HEADER_DEFAULT.format(new Date(time));
	}

}