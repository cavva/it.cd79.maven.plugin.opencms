package it.cd79.maven.plugin.opencms.filenameFilters;

import it.cd79.maven.plugin.opencms.utils.CommonsFiltersUtils;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.IOFileFilter;

public class FileFilter implements IOFileFilter {

	private static FileFilter m_instance;

	private FileFilter() {
	}

	public static FileFilter getInstance() {
		if (m_instance == null) {
			m_instance = new FileFilter();
		}
		return m_instance;
	}

	public boolean accept(File current, String name) {
		return CommonsFiltersUtils.checkFile(FilenameUtils.concat(current.getAbsolutePath(), name), "file.", "denied.file.", false);
	}

	public boolean accept(File file) {
		return CommonsFiltersUtils.checkFile(file.getAbsolutePath(), "file.", "denied.file.", false);
	}

}
