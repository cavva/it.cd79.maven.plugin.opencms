package it.cd79.maven.plugin.opencms.audit;

import it.cd79.maven.plugin.opencms.manifest.model.imports.File;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;

public class AuditCounter {

	private static Log log = new SystemStreamLog();

	private long counter;

	private String counterName;

	public AuditCounter(String counterName) {
		super();
		counter = 0l;
		this.counterName = counterName;
	}

	public void plusOne(File o1, File o2, int result) {
		counter++;
		log.debug(String.format("Counter %s - value [%s] - '%s', '%s' - %d", counterName, counter, o1.getDestination(), o2.getDestination(), result));
	}

	public long getCounter() {
		return counter;
	}

}