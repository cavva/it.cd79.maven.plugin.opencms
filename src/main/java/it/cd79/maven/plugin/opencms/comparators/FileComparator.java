package it.cd79.maven.plugin.opencms.comparators;

import it.cd79.maven.plugin.opencms.audit.AuditCountersFactory;
import it.cd79.maven.plugin.opencms.manifest.model.imports.File;
import it.cd79.maven.plugin.opencms.utils.Configuration;
import it.cd79.maven.plugin.opencms.utils.Constants;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;

import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;

public class FileComparator implements Comparator<File> {
	private static Log LOG = new SystemStreamLog();
	private final static String sepatator = "/|\\\\";

	protected static final int OBJECT_1 = -100;
	protected static final int OBJECT_2 = 100;
	protected static final int EQUALS = 0;
	private static final int DIRECTION = -1;

	@Override
	public int compare(File o1, File o2) {
		int result = compareI(o1, o2);
		AuditCountersFactory.getInstance("FileComparator.compare").plusOne(o1, o2, result);
		return result;
	}

	private int compareI(File o1, File o2) {
		try {
			if (!o1.getType().equalsIgnoreCase(Configuration.getInstance().get(Constants.PROPS_FILE_TYPE_FOLDER))) {
				if (!o2.getType().equalsIgnoreCase(Configuration.getInstance().get(Constants.PROPS_FILE_TYPE_FOLDER))) {
					if (filesHaveSameFolder(o1, o2)) {
						return DIRECTION * o2.getDestination().compareTo(o1.getDestination());
					} else {
						if (parentFolderObject2IsSubfolderOfParentFolderObject1(o1, o2)) {
							return OBJECT_1;
						} else {
							if (parentFolderObject2IsSubfolderOfParentFolderObject1(o2, o1)) {
								return OBJECT_2;
							} else {
								return compareParentFolders(o1, o2);
							}
						}
					}
				} else {
					if (isObject1FolderOfObject2(o2, o1)) {
						return OBJECT_2;
					} else {
						if (compareParentFolders(o1, o2) == EQUALS && filesHaveSameTreeLength(o1, o2)) {
							return OBJECT_1;
						} else {
							return DIRECTION * o2.getDestination().compareTo(o1.getDestination());
						}
					}
				}

			} else {
				if (!o2.getType().equalsIgnoreCase(Configuration.getInstance().get(Constants.PROPS_FILE_TYPE_FOLDER))) {
					if (isObject1FolderOfObject2(o1, o2)) {
						return OBJECT_1;
					} else {
						if (compareParentFolders(o1, o2) == EQUALS && filesHaveSameTreeLength(o1, o2)) {
							return OBJECT_2;
						} else {
							return DIRECTION * o2.getDestination().compareTo(o1.getDestination());
						}
					}
				} else {
					if (isObject1FolderOfObject2(o1, o2)) {
						return OBJECT_1;
					} else {
						if (isObject1FolderOfObject2(o2, o1)) {
							return OBJECT_2;
						} else {
							return DIRECTION * o2.getDestination().compareTo(o1.getDestination());
						}
					}
				}

			}
		} catch (FileNotFoundException e) {
			LOG.error("no configuration loaded", e);
		} catch (IOException e) {
			LOG.error("no configuration loaded", e);
		}
		return 0;
	}

	/**
	 * Check if the folder Object 1, that need to be a folder, is in the tree of Object 2
	 *
	 * @param o1
	 *            File with folder type
	 * @param o2
	 *            File non folder
	 * @return
	 */
	protected static boolean isObject1FolderOfObject2(File o1, File o2) {
		String[] pathList1 = o1.getDestination().split(sepatator);
		String[] pathList2 = o2.getDestination().split(sepatator);

		if (pathList1.length < pathList2.length) {
			for (int i = 0; i < pathList1.length; i++) {
				if (!pathList1[i].equals(pathList2[i]))
					return false;
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if the folder of the object 1 is a parent folder of the object 2
	 *
	 * system is a parent folder of system/modules but not of sites
	 *
	 * @param o1
	 *            First object the main
	 * @param o2
	 *            Second object
	 * @return
	 */
	protected static boolean parentFolderObject2IsSubfolderOfParentFolderObject1(File o1, File o2) {
		String[] pathList1 = o1.getDestination().split(sepatator);
		String[] pathList2 = o2.getDestination().split(sepatator);

		if (pathList1.length < pathList2.length) {
			for (int i = 0; i < pathList1.length - 1; i++) {
				if (!pathList1[i].equals(pathList2[i]))
					return false;
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if object 1 is a parent folder of the object 2
	 *
	 * system is a parent folder of system/modules but not of sites
	 *
	 * @param o1
	 *            File with type folder
	 * @param o2
	 *            Second File with type folder
	 * @return
	 */
	protected static boolean isObject2IsSubfolderOfObject1(File o1, File o2) {
		String[] pathList1 = o1.getDestination().split(sepatator);
		String[] pathList2 = o2.getDestination().split(sepatator);

		if (pathList1.length < pathList2.length) {
			for (int i = 0; i < pathList1.length; i++) {
				if (!pathList1[i].equals(pathList2[i]))
					return false;
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Compare the parent folders each folder name not the full path
	 *
	 * @param o1
	 *            File
	 * @param o2
	 *            File
	 * @return
	 */
	protected static int compareParentFolders(File o1, File o2) {
		String[] pathList1 = o1.getDestination().split(sepatator);
		String[] pathList2 = o2.getDestination().split(sepatator);

		if (pathList1.length < pathList2.length) {
			for (int i = 0; i < pathList1.length - 1; i++) {
				if (!pathList1[i].equals(pathList2[i]))
					return DIRECTION * pathList2[i].compareTo(pathList1[i]);
			}
			return EQUALS;
		} else {
			for (int i = 0; i < pathList2.length - 1; i++) {
				if (!pathList1[i].equals(pathList2[i]))
					return DIRECTION * pathList2[i].compareTo(pathList1[i]);
			}
			return EQUALS;
		}
	}

	/**
	 * Compare the parent folders each folder name not the full path
	 *
	 * @param o1
	 *            File
	 * @param o2
	 *            File
	 * @return
	 */
	protected static int compareFullPath(File o1, File o2) {
		String[] pathList1 = o1.getDestination().split(sepatator);
		String[] pathList2 = o2.getDestination().split(sepatator);

		if (pathList1.length < pathList2.length) {
			for (int i = 0; i < pathList1.length; i++) {
				if (!pathList1[i].equals(pathList2[i]))
					return DIRECTION * pathList2[i].compareTo(pathList1[i]);
			}
			return EQUALS;
		} else {
			for (int i = 0; i < pathList2.length; i++) {
				if (!pathList1[i].equals(pathList2[i]))
					return DIRECTION * pathList2[i].compareTo(pathList1[i]);
			}
			return EQUALS;
		}
	}

	/**
	 * Check if the files are in the same folder
	 *
	 * @param o1
	 *            File
	 * @param o2
	 *            File
	 * @return
	 */
	protected static boolean filesHaveSameFolder(File o1, File o2) {
		String[] pathList1 = o1.getDestination().split(sepatator);
		String[] pathList2 = o2.getDestination().split(sepatator);

		if (pathList1.length == pathList2.length) {
			for (int i = 0; i < pathList2.length - 1; i++) {
				if (!pathList1[i].equals(pathList2[i]))
					return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Check if the files have the same tree length
	 *
	 * @param o1
	 *            File
	 * @param o2
	 *            File
	 * @return
	 */
	protected static boolean filesHaveSameTreeLength(File o1, File o2) {
		String[] pathList1 = o1.getDestination().split(sepatator);
		String[] pathList2 = o2.getDestination().split(sepatator);

		if (pathList1.length == pathList2.length) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Tree length difference
	 *
	 * @param o1
	 *            File
	 * @param o2
	 *            File
	 * @return
	 */
	protected static int shortestPathDepth(File o1, File o2) {
		String[] pathList1 = o1.getDestination().split(sepatator);
		String[] pathList2 = o2.getDestination().split(sepatator);

		return pathList2.length - pathList1.length;
	}
}