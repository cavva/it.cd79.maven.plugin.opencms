package it.cd79.maven.plugin.opencms.utils;

public class Constants {
	public static final String PROPS_DEFAULT_EXPORT_FILES_FILE_USERCREATED = "default.export.files.file.usercreated";
	public static final String PROPS_DEFAULT_EXPORT_FILES_FILE_USERLASTMODIFIED = "default.export.files.file.userlastmodified";
	public static final String PROPS_DEFAULT_EXPORT_FILES_FILE_FLAGS = "default.export.files.file.flags";
	public static final String PROPS_FILE_TYPE_FOLDER = "ft.folder";
	public static final String PROPS_FILE_ACL_FILE_EXTENSION = "file.acl.file.extension";
	public static final String PROPS_FILE_PROPERTIES_FILE_EXTENSION = "file.properties.file.extension";
	public static final String PROPS_FILE_ATTRIBUTES_FILE_EXTENSION = "file.attributes.file.extension";
	public static final String PROPS_FILE_INFO_MODULE_ATTRIBUTES_FILE_EXTENSION = "file.module.attributes.info.file.extension";
	public static final String PROPS_FILE_MODULE_MODULE_ATTRIBUTES_FILE_EXTENSION = "file.module.attributes.module.file.extension";
	public static final String PROPS_FILE_PROJECT_MODULE_ATTRIBUTES_FILE_EXTENSION = "file.module.attributes.project.file.extension";
	public static final String PROPS_FILE_ACCOUNTS_MODULE_ATTRIBUTES_FILE_EXTENSION = "file.module.attributes.accounts.file.extension";
	public static final String PREFIX_DENIED_DIRECTORIES = "denied.directory.";
	public static final String PREFIX_DENIED_FILES = "denied.file.";

}
