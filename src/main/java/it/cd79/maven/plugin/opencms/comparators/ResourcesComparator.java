package it.cd79.maven.plugin.opencms.comparators;

import it.cd79.maven.plugin.opencms.manifest.model.modules.Resource;

import java.util.Comparator;

public class ResourcesComparator implements Comparator<Resource> {

	public int compare(Resource o1, Resource o2) {
		return o1.getUri().compareTo(o2.getUri());
	}

}
