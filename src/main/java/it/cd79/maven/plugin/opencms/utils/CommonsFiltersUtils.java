package it.cd79.maven.plugin.opencms.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;

public class CommonsFiltersUtils {
	private static Log	LOG	= new SystemStreamLog();

	/**
	 * The logic is the properties file conf.properties has the properties
	 * structured like:
	 * <p>
	 * <ul>
	 * <li><b>file.</b>acl.file.extension=.opencms-acl
	 * <li><b>file.</b>properties.file.extension=.opencms-props
	 * <li><b>file.</b>attributes.file.extension=.opencms-attrs
	 * <li>
	 * <i><b>file.</b>module.attributes.</i>info.file.extension=info.opencms-
	 * module- attributes
	 * <li><i><b>file.</b>module.attributes.</i>module.file.extension=module.
	 * opencms-module- attributes
	 * <li><i><b>file.</b>module.attributes.</i>project.file.extension=project.
	 * opencms-module- attributes
	 * <li>
	 * <i><b>file.</b>module.attributes.</i>accounts.file.extension=accounts.
	 * opencms-module -attributes
	 * </ul>
	 * 
	 * with those prefix, the bold and the italic you can have an example of
	 * element that are checked. If you use "file." as a prefix all elements are
	 * checked, if one of them are matching as a end or fullname it return
	 * include.
	 * 
	 * include means if the element should be considered (include = true) or
	 * excluded (include = false)
	 * 
	 * @param name
	 *            full name / path of the file included extension
	 * @param prefix
	 *            prefix of the parameter to include
	 * @param include
	 *            prefix of the parameter to exclude
	 * @param deniedPrefix
	 * @return <b>true</b> if is to include, <b>false</b> if to discard
	 */
	public static boolean checkFile(String name, String prefix, String deniedPrefix, boolean include) {
		// Check if it is a file, if it is a directory i don't need it
		String uniformedName = FilenameUtils.separatorsToUnix(name);
		File file = new File(name);
		if (!file.isFile()) {
			return false;
		}

		Configuration confProps;
		try {
			confProps = Configuration.getInstance();
		} catch (FileNotFoundException e) {
			LOG.error("The configuration file is not found", e);
			return false;
		} catch (IOException e) {
			LOG.error("Error reading configuration file", e);
			return false;
		}

		// Check if it is a denied file
		Map<String, String> denied = confProps.getFileGroupsElements(deniedPrefix);
		for (String key : denied.keySet()) {
			if (uniformedName.endsWith(denied.get(key))) {
				return false;
			}
		}

		// Check if it is one of the configuration / properties file
		Map<String, String> props = confProps.getFileGroupsElements(prefix);
		for (String key : props.keySet()) {
			if (uniformedName.endsWith(props.get(key))) {
				return include;
			}
		}
		return !include;
	}

	public static boolean checkDirectory(String name, String deniedPrefix) {
		File file = new File(name);
		// Check if it is not a directory then I'll return false
		if (!file.isDirectory()) {
			return false;
		}

		Configuration confProps;
		try {
			confProps = Configuration.getInstance();
		} catch (FileNotFoundException e) {
			LOG.error("The configuration file is not found", e);
			return false;
		} catch (IOException e) {
			LOG.error("Error reading configuration file", e);
			return false;
		}

		String uniformedName = FilenameUtils.separatorsToUnix(name);
		// Check if it is a denied file
		Map<String, String> denied = confProps.getFileGroupsElements(deniedPrefix);
		for (String key : denied.keySet()) {
			if (uniformedName.endsWith(denied.get(key))) {
				return false;
			}
		}
		return true;
	}

	public static Collection<File> postFilter(Collection<File> fNd, IOFileFilter filter) {
		if ((fNd == null) || (fNd.size() < 1) || (filter == null)) {
			return null;
		}

		Collection<File> res = new ArrayList<File>();
		for (File file : fNd) {
			if (filter.accept(file)) {
				res.add(file);
			}
		}
		return res;
	}
}
