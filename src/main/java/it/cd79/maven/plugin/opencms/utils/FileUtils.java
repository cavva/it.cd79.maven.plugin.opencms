package it.cd79.maven.plugin.opencms.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.IOFileFilter;

public class FileUtils {

	public static Collection<File> findFilesAndDirsPostFiltered(File root, IOFileFilter filter) {
		if (root == null) {
			return null;
		}
		String[] list = root.list();
		if ((list == null) || (list.length < 1)) {
			return Collections.emptyList();
		}
		Collection<File> res = new ArrayList<File>();
		for (String f : list) {
			File file = new File(FilenameUtils.concat(root.getAbsolutePath(), f));
			if (filter.accept(file)) {
				res.add(file);
			}
			if (file.isDirectory()) {
				res.addAll(findFilesAndDirsPostFiltered(file, filter));
			}
		}
		return res;
	}
}
