package it.cd79.maven.plugin.opencms.filenameFilters;

import it.cd79.maven.plugin.opencms.utils.CommonsFiltersUtils;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.IOFileFilter;

public class MainAttributesFilter implements IOFileFilter {

	private static MainAttributesFilter m_instance;

	private MainAttributesFilter() {
	}

	public static MainAttributesFilter getInstance() {
		if (m_instance == null) {
			m_instance = new MainAttributesFilter();
		}
		return m_instance;
	}

	public boolean accept(File current, String name) {
		return CommonsFiltersUtils.checkFile(FilenameUtils.concat(current.getAbsolutePath(), name), "file.module.attributes.", "denied.file.", true);
	}

	public boolean accept(File file) {
		return CommonsFiltersUtils.checkFile(file.getAbsolutePath(), "file.module.attributes.", "denied.file.", true);
	}

}
