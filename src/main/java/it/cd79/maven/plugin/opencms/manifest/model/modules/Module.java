//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.26 at 07:45:19 PM CEST 
//

package it.cd79.maven.plugin.opencms.manifest.model.modules;

import it.cd79.maven.plugin.opencms.manifest.model.resourcetypes.Resourcetypes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.eclipse.persistence.oxm.annotations.XmlCDATA;

/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "name", "nicename", "group", "clazz", "description", "version", "authorname", "authoremail", "datecreated", "userinstalled",
		"dateinstalled", "dependencies", "exportpoints", "resources", "parameters", "resourcetypes", "explorertypes" })
@XmlRootElement(name = "module")
public class Module {

	@XmlElement(required = true)
	protected String		name;
	@XmlCDATA
	protected String		nicename;
	protected String		group;
	@XmlElement(name = "class")
	protected String		clazz;
	protected String		description;
	@XmlElement(required = true)
	protected String		version;
	@XmlCDATA
	protected String		authorname;
	@XmlCDATA
	protected String		authoremail;
	protected String		datecreated;
	protected String		userinstalled;
	protected String		dateinstalled;
	protected Dependencies	dependencies;
	protected Exportpoints	exportpoints;
	protected Resources		resources;
	protected Parameters	parameters;
	protected Resourcetypes	resourcetypes;
	protected Explorertypes	explorertypes;

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the nicename property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNicename() {
		return nicename;
	}

	/**
	 * Sets the value of the nicename property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNicename(String value) {
		this.nicename = value;
	}

	/**
	 * Gets the value of the group property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Sets the value of the group property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setGroup(String value) {
		this.group = value;
	}

	/**
	 * Gets the value of the clazz property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClazz() {
		return clazz;
	}

	/**
	 * Sets the value of the clazz property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setClazz(String value) {
		this.clazz = value;
	}

	/**
	 * Gets the value of the description property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the description property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDescription(String value) {
		this.description = value;
	}

	/**
	 * Gets the value of the version property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the value of the version property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVersion(String value) {
		this.version = value;
	}

	/**
	 * Gets the value of the authorname property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthorname() {
		return authorname;
	}

	/**
	 * Sets the value of the authorname property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAuthorname(String value) {
		this.authorname = value;
	}

	/**
	 * Gets the value of the authoremail property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthoremail() {
		return authoremail;
	}

	/**
	 * Sets the value of the authoremail property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAuthoremail(String value) {
		this.authoremail = value;
	}

	/**
	 * Gets the value of the datecreated property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDatecreated() {
		return datecreated;
	}

	/**
	 * Sets the value of the datecreated property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDatecreated(String value) {
		this.datecreated = value;
	}

	/**
	 * Gets the value of the userinstalled property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getUserinstalled() {
		return userinstalled;
	}

	/**
	 * Sets the value of the userinstalled property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setUserinstalled(String value) {
		this.userinstalled = value;
	}

	/**
	 * Gets the value of the dateinstalled property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateinstalled() {
		return dateinstalled;
	}

	/**
	 * Sets the value of the dateinstalled property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDateinstalled(String value) {
		this.dateinstalled = value;
	}

	/**
	 * Gets the value of the dependencies property.
	 * 
	 * @return possible object is {@link Dependencies }
	 * 
	 */
	public Dependencies getDependencies() {
		return dependencies;
	}

	/**
	 * Sets the value of the dependencies property.
	 * 
	 * @param value
	 *            allowed object is {@link Dependencies }
	 * 
	 */
	public void setDependencies(Dependencies value) {
		this.dependencies = value;
	}

	/**
	 * Gets the value of the exportpoints property.
	 * 
	 * @return possible object is {@link Exportpoints }
	 * 
	 */
	public Exportpoints getExportpoints() {
		return exportpoints;
	}

	/**
	 * Sets the value of the exportpoints property.
	 * 
	 * @param value
	 *            allowed object is {@link Exportpoints }
	 * 
	 */
	public void setExportpoints(Exportpoints value) {
		this.exportpoints = value;
	}

	/**
	 * Gets the value of the resources property.
	 * 
	 * @return possible object is {@link Resources }
	 * 
	 */
	public Resources getResources() {
		return resources;
	}

	/**
	 * Sets the value of the resources property.
	 * 
	 * @param value
	 *            allowed object is {@link Resources }
	 * 
	 */
	public void setResources(Resources value) {
		this.resources = value;
	}

	/**
	 * Gets the value of the parameters property.
	 * 
	 * @return possible object is {@link Parameters }
	 * 
	 */
	public Parameters getParameters() {
		return parameters;
	}

	/**
	 * Sets the value of the parameters property.
	 * 
	 * @param value
	 *            allowed object is {@link Parameters }
	 * 
	 */
	public void setParameters(Parameters value) {
		this.parameters = value;
	}

	/**
	 * Gets the value of the resourcetypes property.
	 * 
	 * @return possible object is {@link Resourcetypes }
	 * 
	 */
	public Resourcetypes getResourcetypes() {
		return resourcetypes;
	}

	/**
	 * Sets the value of the resourcetypes property.
	 * 
	 * @param value
	 *            allowed object is {@link Resourcetypes }
	 * 
	 */
	public void setResourcetypes(Resourcetypes value) {
		this.resourcetypes = value;
	}

	/**
	 * Gets the value of the explorertypes property.
	 * 
	 * @return possible object is {@link Explorertypes }
	 * 
	 */
	public Explorertypes getExplorertypes() {
		return explorertypes;
	}

	/**
	 * Sets the value of the explorertypes property.
	 * 
	 * @param value
	 *            allowed object is {@link Explorertypes }
	 * 
	 */
	public void setExplorertypes(Explorertypes value) {
		this.explorertypes = value;
	}

}
