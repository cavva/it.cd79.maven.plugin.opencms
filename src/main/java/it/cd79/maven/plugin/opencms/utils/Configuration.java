package it.cd79.maven.plugin.opencms.utils;

import it.cd79.maven.plugin.opencms.exceptions.NoFileTypeException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

public class Configuration {

	private static Configuration m_instance;
	private final Properties m_props;

	private Configuration(Map<String, String> props) throws FileNotFoundException, IOException {
		m_props = new Properties();
		load(props);
	}

	private void load(Map<String, String> props) throws IOException, FileNotFoundException {
		m_props.load(this.getClass().getResourceAsStream("/conf.properties"));
		if (props != null)
			m_props.putAll(props);
	}

	public static Configuration getInstance() throws FileNotFoundException, IOException {
		return getInstance(null);
	}

	public static Configuration getInstance(Map<String, String> props) throws FileNotFoundException, IOException {
		if (m_instance == null) {
			m_instance = new Configuration(props);
		}
		return m_instance;
	}

	public void reload(Map<String, String> props) throws FileNotFoundException, IOException {
		load(props);
	}

	/**
	 * Search on the conf.properties file the configuration string
	 * 
	 * @param prop
	 * @return String of the property
	 */
	public String get(String prop) {
		return m_props.getProperty(prop);
	}

	/**
	 * Search on the conf.properties file the ft.&lt;extension&gt; and return it if present else throw an exception
	 * 
	 * @param filename
	 *            of the file with extension
	 * @return the file type of opencms
	 * @throws NoFileTypeException
	 */
	public String getFileTypeFromExtension(String filename) throws NoFileTypeException {
		File file = new File(filename);
		if (file.isDirectory() || !file.exists()) {
			return m_props.getProperty(Constants.PROPS_FILE_TYPE_FOLDER);
		}
		String ext = FilenameUtils.getExtension(filename);
		String fileType = m_props.getProperty("ft." + ext);
		if (StringUtils.isBlank(fileType)) {
			throw new NoFileTypeException("File " + filename + " does not has file type with extension \"" + ext
					+ "\" does not exists in \"conf.properties\", PLEASE ADD \"ft." + ext + "=<your file type>\"");
		}
		return fileType;
	}

	/**
	 * Search on the conf.properties file the default.properties.&lt;extension&gt; and return it if present else throw an exception
	 * 
	 * @param filename
	 *            of the file with extension
	 * @return the file properties of opencms
	 * @throws NoFileTypeException
	 */
	public String getDefaultFilePropertiesFromExtension(String filename) throws NoFileTypeException {
		if (new File(filename).isDirectory()) {
			return m_props.getProperty(Constants.PROPS_FILE_TYPE_FOLDER);
		}
		String ext = FilenameUtils.getExtension(filename);
		String fileType = m_props.getProperty("default.properties." + ext);
		if (StringUtils.isBlank(fileType)) {
			throw new NoFileTypeException("File " + filename + " does not has file type with extension \"" + ext
					+ "\" does not exists in \"conf.properties\", PLEASE ADD \"ft." + ext + "=<your file type>\"");
		}
		return fileType;
	}

	/**
	 * Search on the conf.properties file the default.properties.&lt;extension&gt; and return true if present else false
	 * 
	 * @param filename
	 *            of the file with extension
	 * @return present or not
	 */
	public boolean hasDefaultProperties(String filename) {
		if (new File(filename).isDirectory()) {
			return m_props.containsKey("default.properties." + Constants.PROPS_FILE_TYPE_FOLDER);
		}
		String ext = FilenameUtils.getExtension(filename);
		return m_props.containsKey("default.properties." + ext);
	}

	/**
	 * find all Properties with the prefix. It is case sensitive!!!
	 * 
	 * @param prefix
	 *            prefix to group
	 * @return all properties grouped in a Map&lt;String, String&gt;
	 */
	public Map<String, String> getFileGroupsElements(String prefix) {
		Map<String, String> res = new TreeMap<String, String>();
		for (Object key : m_props.keySet()) {
			if (key instanceof String) {
				String skey = (String) key;
				if (skey.startsWith(prefix)) {
					res.put(skey, m_props.getProperty(skey));
				}
			}
		}
		if (res.isEmpty()) {
			return null;
		}
		return res;
	}
}
