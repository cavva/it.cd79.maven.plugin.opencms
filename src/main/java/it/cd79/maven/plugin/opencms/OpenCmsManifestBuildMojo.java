package it.cd79.maven.plugin.opencms;

import it.cd79.maven.plugin.opencms.comparators.ExportpointComparator;
import it.cd79.maven.plugin.opencms.comparators.FileComparator;
import it.cd79.maven.plugin.opencms.comparators.NaturalOrderFileComparator;
import it.cd79.maven.plugin.opencms.exceptions.NoFileTypeException;
import it.cd79.maven.plugin.opencms.filenameFilters.DirectoryFilter;
import it.cd79.maven.plugin.opencms.filenameFilters.FileFilter;
import it.cd79.maven.plugin.opencms.filenameFilters.MainAttributesFilter;
import it.cd79.maven.plugin.opencms.manifest.model.imports.Accesscontrol;
import it.cd79.maven.plugin.opencms.manifest.model.imports.Accounts;
import it.cd79.maven.plugin.opencms.manifest.model.imports.Export;
import it.cd79.maven.plugin.opencms.manifest.model.imports.Files;
import it.cd79.maven.plugin.opencms.manifest.model.imports.Info;
import it.cd79.maven.plugin.opencms.manifest.model.imports.ObjectFactory;
import it.cd79.maven.plugin.opencms.manifest.model.imports.Projects;
import it.cd79.maven.plugin.opencms.manifest.model.imports.Properties;
import it.cd79.maven.plugin.opencms.manifest.model.modules.Exportpoint;
import it.cd79.maven.plugin.opencms.manifest.model.modules.Module;
import it.cd79.maven.plugin.opencms.manifest.model.modules.Resource;
import it.cd79.maven.plugin.opencms.utils.Configuration;
import it.cd79.maven.plugin.opencms.utils.Constants;
import it.cd79.maven.plugin.opencms.utils.FileUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.opencms.util.CmsDateUtil;

@Mojo(name = "manifest", defaultPhase = LifecyclePhase.PREPARE_PACKAGE, executionStrategy = "always")
@Execute(goal = "manifest", phase = LifecyclePhase.PREPARE_PACKAGE)
public class OpenCmsManifestBuildMojo extends AbstractMojo {
	private static Log log = new SystemStreamLog();

	/**
	 * Output directory if different from <b>target/opencms-pre-build</b>
	 */
	@Parameter(defaultValue = "target/opencms-pre-build", required = true)
	protected String outputDir;

	/**
	 * Directory of the built classes. Generally maven uses <b>target/classes</b>
	 */
	@Parameter(defaultValue = "target/classes", required = false)
	protected String inputClassesDir;

	/**
	 * Directory where opencms-maven-plugin will copy all libraries not provided and not for test. The default is <b>target/copied-lib</b>
	 */
	@Parameter(defaultValue = "target/copied-lib", required = false)
	protected String inputLibsDir;

	/**
	 * Directory of the resources of the module you are building.
	 * <p>
	 * <p>
	 * I separated those the directories under the <i>src/main/opencms</i> in this case <b>src/main/opencms/module</b>
	 */
	@Parameter(defaultValue = "src/main/opencms/module", required = false)
	protected String inputOpencmsModuleDir;

	/**
	 * Directory of the xml you can customize the part info/module/project/accounts of the manifest.xml.
	 * <p>
	 * <p>
	 * I separated those the directories under the <i>src/main/opencms</i> in this case <b>src/main/opencms/module-attribute</b> and the files should be named:
	 * <ul>
	 * <li>info.opencms-module-attributes</li>
	 * <li>module.opencms-module-attributes</li>
	 * <li>project.opencms-module-attributes</li>
	 * <li>accounts.opencms-module-attributes</li>
	 * </ul>
	 *
	 * those file aren't essential, I tried to keep simplicity and the minimal requirement are inserted automatically, you just need to fill the maven pom.xml
	 * in the required element. See the wiki.
	 *
	 * The ful syntax is not yet clear to me, but I'm going to generate a full file of every of them.
	 */
	@Parameter(defaultValue = "src/main/opencms/module-attribute", required = true)
	protected String inputOpencmsModuleAttributeDir;

	/**
	 * Directory of the resources outside the module subirectories tree. You can put everything is in the VFS of opencms here. I noticed that if you put
	 * something such root/sites/default you have to import it with selected site = / .
	 * <p>
	 * This feature is supposed to fill the gap i found in other developer tools. If you need to build a new view, you must write code on the system/workplace
	 * directory.
	 * <p>
	 * <p>
	 * I separated those the directories under the <i>src/main/opencms</i> in this case <b>src/main/opencms/root</b>
	 */
	@Parameter(defaultValue = "src/main/opencms/root", required = false)
	protected String inputOpencmsRootDir;

	/**
	 * Special folder used to add elements in the META-INF webapp folder
	 * <p>
	 * This feature is supposed to fill the gap i found in other developer tools. This is useful if you need to add frameworks such spring.
	 * <p>
	 * <p>
	 * Use folder path such <b>src/main/opencms/meta-inf</b>.
	 */
	@Parameter(required = false)
	protected String inputWebappMetaInfDir;

	@Parameter
	protected Map<String, String> properties;

	@Component
	protected MavenProject project;

	private static ObjectFactory OF_IMPORT;
	private static it.cd79.maven.plugin.opencms.manifest.model.modules.ObjectFactory OF_MODULES;

	public OpenCmsManifestBuildMojo() {
		super();
		OF_IMPORT = new ObjectFactory();
		OF_MODULES = new it.cd79.maven.plugin.opencms.manifest.model.modules.ObjectFactory();
		setLog(log);
	}

	/**
	 * Generate the manifest.xml
	 *
	 * @param classes
	 *            {@link File} of the root directory for the classes {@link OpenCmsManifestBuildMojo#inputClassesDir}
	 * @param module
	 *            {@link File} of the root directory for the module resources such tempalates / elements / resources / schemas ecc...
	 *            {@link OpenCmsManifestBuildMojo#inputOpencmsModuleDir}
	 * @param moduleAttributes
	 *            {@link File} of the root directory for the {@link OpenCmsManifestBuildMojo#inputOpencmsModuleAttributeDir}
	 * @param opencmsRoot
	 *            {@link File} of the root directory for the {@link OpenCmsManifestBuildMojo#inputOpencmsRootDir}
	 * @param libsCopied
	 *            {@link File} of the root directory for the {@link OpenCmsManifestBuildMojo#inputLibsDir}
	 * @param metaInf
	 *            {@link File} eventually META-INF directory {@link OpenCmsManifestBuildMojo#inputWebappMetaInfDir}
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws JAXBException
	 * @throws MojoFailureException
	 */
	public void generateManifest(File classes, File module, File moduleAttributes, File opencmsRoot, File libsCopied, File metaInf)
			throws FileNotFoundException, IOException, JAXBException, MojoFailureException {
		String moduleName = project.getGroupId() + "." + project.getArtifactId();
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

		final File root = new File("");
		Configuration p;
		p = Configuration.getInstance();

		// I find the module attributes files
		Collection<File> moduleAttrs = FileUtils.findFilesAndDirsPostFiltered(moduleAttributes, MainAttributesFilter.getInstance());
		Export export;
		try {
			export = fillInfoAndModule(moduleAttrs);
		} catch (FileNotFoundException e) {
			log.error("Error", e);
			throw e;
		} catch (IOException e) {
			log.error("Error", e);
			throw e;
		} catch (JAXBException e) {
			log.error("Error", e);
			throw e;
		}

		checkRequiredXmlElements(export);
		Date now = new Date();

		Module moduleXml = fillModule(moduleName, p, export);
		fillInfo(export, now);

		Files files = OF_IMPORT.createFiles();

		Collection<File> baseRoot = new ArrayList<File>() {
			private static final long serialVersionUID = 3426849310859331328L;

			{
				add(root);
			}
		};

		fillFiles(root, "system/modules/" + moduleName, p, baseRoot, files, now);

		// Add the classes if needed
		if (classes != null) {
			Collection<File> fileAndDirsClasses = new ArrayList<File>();
			String classesUri = "system/modules/" + moduleName + "/classes";
			fileAndDirsClasses.add(classes);
			fileAndDirsClasses.addAll(FileUtils.findFilesAndDirsPostFiltered(classes, DirectoryFilter.getInstance()));
			fileAndDirsClasses.addAll(FileUtils.findFilesAndDirsPostFiltered(classes, FileFilter.getInstance()));
			fillFiles(classes, classesUri, p, fileAndDirsClasses, files, now);
			Exportpoint e = new Exportpoint();
			e.setDestination("WEB-INF/classes/");
			e.setUri("/" + classesUri + "/");
			moduleXml.getExportpoints().getExportpoint().add(e);
		}

		// Add module jsps, resources and schemas
		if (module != null) {
			Collection<File> fileAndDirsModule = new ArrayList<File>();
			fileAndDirsModule.addAll(FileUtils.findFilesAndDirsPostFiltered(module, DirectoryFilter.getInstance()));
			fileAndDirsModule.addAll(FileUtils.findFilesAndDirsPostFiltered(module, FileFilter.getInstance()));
			fillFiles(module, "system/modules/" + moduleName, p, fileAndDirsModule, files, now);
		}

		// Add subdirectories of the root if needed such /system/workplace or
		// even /sites/default
		if (opencmsRoot != null) {
			Collection<File> fileAndDirsOpencmsRoot = FileUtils.findFilesAndDirsPostFiltered(opencmsRoot, DirectoryFilter.getInstance());

			for (File file : fileAndDirsOpencmsRoot) {
				String relativeFilePath = file.getAbsolutePath().substring(opencmsRoot.getAbsolutePath().length());
				Resource r = new Resource();
				r.setUri(FilenameUtils.concat(relativeFilePath, ""));
				moduleXml.getResources().getResource().add(r);
			}

			Collection<File> fs = FileUtils.findFilesAndDirsPostFiltered(opencmsRoot, FileFilter.getInstance());
			fileAndDirsOpencmsRoot.addAll(fs);
			fillFiles(opencmsRoot, "", p, fileAndDirsOpencmsRoot, files, now);
		}

		// Add Libraries either subdependencies
		if (libsCopied != null) {
			Collection<File> libFiles = new ArrayList<File>();
			String libPath = "system/modules/" + moduleName + "/lib";

			libFiles.add(libsCopied);
			libFiles.addAll(FileUtils.findFilesAndDirsPostFiltered(libsCopied, DirectoryFilter.getInstance()));
			libFiles.addAll(FileUtils.findFilesAndDirsPostFiltered(libsCopied, FileFilter.getInstance()));

			if (libFiles.size() > 1) {
				fillFiles(libsCopied, libPath, p, libFiles, files, now);
				Exportpoint e = new Exportpoint();
				e.setDestination("WEB-INF/lib/");
				e.setUri("/" + libPath + "/");
				moduleXml.getExportpoints().getExportpoint().add(e);
			}

		}

		// Add meta-inf
		if (metaInf != null) {
			Collection<File> fileAndDirsMetaInf = new ArrayList<File>();
			String metaInfUri = "system/modules/" + moduleName + "/meta-inf";
			fileAndDirsMetaInf.add(metaInf);
			fileAndDirsMetaInf.addAll(FileUtils.findFilesAndDirsPostFiltered(metaInf, DirectoryFilter.getInstance()));
			fileAndDirsMetaInf.addAll(FileUtils.findFilesAndDirsPostFiltered(metaInf, FileFilter.getInstance()));
			fillFiles(metaInf, metaInfUri, p, fileAndDirsMetaInf, files, now);

			if (fileAndDirsMetaInf.size() > 1) {
				Exportpoint e = new Exportpoint();
				e.setDestination("META-INF/");
				e.setUri("/" + metaInfUri + "/");
				moduleXml.getExportpoints().getExportpoint().add(e);
			}

		}

		// if (log.isDebugEnabled())
		for (int i = 0; i < files.getFile().size(); i++) {
			log.debug(String.format("File %d\t-\tPath: %s", i + 1, files.getFile().get(i).getDestination()));
		}

//		Collections.sort(files.getFile(), new NaturalOrderFileComparator());
//
//		for (int i = 0; i < files.getFile().size(); i++) {
//			log.debug(String.format("File %d\t-\tPath: %s", i + 1, files.getFile().get(i).getDestination()));
//		}

		Collections.sort(export.getModule().getExportpoints().getExportpoint(), new ExportpointComparator());

		//Collections.sort(files.getFile(), new FileComparator());

		export.setFiles(files);

		JAXBContext jc = JAXBContext.newInstance(Export.class);

		log.info("JAXB CLASS : " + jc.getClass());

		Marshaller jm = jc.createMarshaller();

		// prettify xml output
		jm.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		File output = new File(outputDir);
		output.mkdirs();
		File file = new File(FilenameUtils.concat(outputDir, "manifest.xml"));
		jm.marshal(export, file);
		log.info("Created manifest.xml in position: " + file.getAbsolutePath());
		// org.apache.commons.io.FileUtils.deleteDirectory(new File("system"));
	}

	/**
	 * Fill the basic info of the manifest.xml
	 *
	 * @param export
	 * @param now
	 */
	private void fillInfo(Export export, Date now) {
		Info infoXml = export.getInfo();

		// Set opencms_version if not present on info.opencms-module-attributes
		if (StringUtils.isBlank(infoXml.getOpencmsVersion())) {
			infoXml.setOpencmsVersion(getOpencmsCoreDependencyVersion());
		}

		// Set createdate to now if not present other
		if (StringUtils.isBlank(infoXml.getCreatedate())) {
			infoXml.setCreatedate(CmsDateUtil.getHeaderDate(now.getTime()));
		}

		// Set infoproject to Offline if not present on
		// info.opencms-module-attributes
		if (StringUtils.isBlank(infoXml.getInfoproject())) {
			infoXml.setInfoproject("Offline");
		}

		// Set export_version to 7 if not present on
		// info.opencms-module-attributes
		if (StringUtils.isBlank(infoXml.getExportVersion())) {
			infoXml.setExportVersion("7");
		}

		// Set creator to Admin if not present on info.opencms-module-attributes
		if (StringUtils.isBlank(infoXml.getCreator())) {
			infoXml.setCreator("Admin");
		}
	}

	private Module fillModule(String moduleName, Configuration p, Export export) throws MojoFailureException {
		// check if there is exportpoints or not
		checkExportpoints(p, export);
		Module moduleXml = export.getModule();
		// Fill version
		if (StringUtils.isBlank(moduleXml.getVersion())) {
			moduleXml.setVersion(project.getVersion().replaceAll("[^0-9.]", ""));
		}
		// Fill name
		if (StringUtils.isBlank(moduleXml.getName())) {
			moduleXml.setName(moduleName);
		}

		// Fill nicename
		if (StringUtils.isBlank(moduleXml.getNicename())) {
			moduleXml.setNicename(StringUtils.isNotBlank(project.getName()) ? project.getName() : "");
		}

		// Fill description
		if (StringUtils.isBlank(moduleXml.getDescription())) {
			moduleXml.setDescription(StringUtils.isNotBlank(project.getDescription()) ? project.getDescription() : "");
		}

		// Fill datecreated
		if (StringUtils.isBlank(moduleXml.getDatecreated())) {
			moduleXml.setDatecreated("");
		}

		// Fill class
		if (StringUtils.isBlank(moduleXml.getClazz())) {
			moduleXml.setClazz("");
		}

		// Fill group
		/*
		 * if (StringUtils.isBlank(moduleXml.getGroup())) { moduleXml.setGroup(""); }
		 */

		// Fill datecreated
		if (StringUtils.isBlank(moduleXml.getDatecreated())) {
			moduleXml.setDatecreated("");
			;
		}

		// Fill Userinstalled
		if (StringUtils.isBlank(moduleXml.getUserinstalled())) {
			moduleXml.setUserinstalled("");
		}

		// Fill Dateinstalled
		if (StringUtils.isBlank(moduleXml.getDateinstalled())) {
			moduleXml.setDateinstalled("");
		}

		// Fill Dependencies
		if (moduleXml.getDependencies() == null) {
			moduleXml.setDependencies(OF_MODULES.createDependencies());
		}

		// First developer is the developer responsible for the project and the
		// owner of the module
		if ((project.getDevelopers() != null) && (project.getDevelopers().size() > 0)
				&& (StringUtils.isBlank(moduleXml.getAuthoremail()) && StringUtils.isBlank(moduleXml.getAuthorname()))) {
			moduleXml.setAuthoremail(project.getDevelopers().get(0).getEmail());
			moduleXml.setAuthorname(project.getDevelopers().get(0).getName());
		} else {
			moduleXml.setAuthoremail("");
			moduleXml.setAuthorname("");
		}

		// Fill parameters
		if (moduleXml.getParameters() == null) {
			moduleXml.setParameters(OF_MODULES.createParameters());
		}

		// check if resources is initialized
		checkModuleResources(p, export);

		// Add the default resource /system/modules/xxxxxxxxxxx
		Resource resource = new Resource();
		resource.setUri("/system/modules/" + moduleName + "/");
		moduleXml.getResources().getResource().add(resource);

		return moduleXml;
	}

	private void checkRequiredXmlElements(Export export) {
		if (export.getInfo() == null) {
			export.setInfo(OF_IMPORT.createInfo());
		}
		if (export.getModule() == null) {
			export.setModule(OF_MODULES.createModule());
		}
	}

	/**
	 * return the project dependency version of opencms-core of groupId org.opencms or the default 7.0.0 as imposed for version 7 import type
	 *
	 * @return opencms-core version if not specified will return version 7.0.0
	 */
	public String getOpencmsCoreDependencyVersion() {
		for (Dependency d : project.getDependencies()) {
			if (d.getArtifactId().trim().equalsIgnoreCase("opencms-core") && d.getGroupId().trim().equalsIgnoreCase("org.opencms")) {
				return d.getVersion();
			}
		}
		return "7.0.0";
	}

	private void checkExportpoints(Configuration p, Export export) throws MojoFailureException {
		if (export.getModule() == null) {
			throw new MojoFailureException("you must define the " + p.get(Constants.PROPS_FILE_MODULE_MODULE_ATTRIBUTES_FILE_EXTENSION));
		}
		if (export.getModule().getExportpoints() == null) {
			export.getModule().setExportpoints(OF_MODULES.createExportpoints());
		}
	}

	private void checkModuleResources(Configuration p, Export export) throws MojoFailureException {
		if (export.getModule() == null) {
			throw new MojoFailureException("you must define the " + p.get(Constants.PROPS_FILE_MODULE_MODULE_ATTRIBUTES_FILE_EXTENSION));
		}
		if (export.getModule().getResources() == null) {
			export.getModule().setResources(OF_MODULES.createResources());
		}
	}

	private Files fillFiles(File root, String baseRoot, Configuration p, Collection<File> fileAndDirs, Files files, Date now) throws JAXBException {
		for (File file : fileAndDirs) {
			it.cd79.maven.plugin.opencms.manifest.model.imports.File f = OF_IMPORT.createFile();
			String relativeFilePath = FilenameUtils.concat(
					baseRoot,
					file.getAbsolutePath().substring(
							root.getAbsolutePath().length() + (root.getAbsolutePath().length() < file.getAbsolutePath().length() ? 1 : 0)));
			relativeFilePath = FilenameUtils.normalizeNoEndSeparator(relativeFilePath);
			relativeFilePath = FilenameUtils.separatorsToUnix(relativeFilePath);
			File acl = new File(file.getAbsolutePath() + p.get(Constants.PROPS_FILE_ACL_FILE_EXTENSION));
			File attrs = new File(file.getAbsolutePath() + p.get(Constants.PROPS_FILE_ATTRIBUTES_FILE_EXTENSION));
			File props = new File(file.getAbsolutePath() + p.get(Constants.PROPS_FILE_PROPERTIES_FILE_EXTENSION));
			if (attrs.exists()) {
				JAXBContext jc = JAXBContext.newInstance(it.cd79.maven.plugin.opencms.manifest.model.imports.File.class);
				Unmarshaller jum = jc.createUnmarshaller();
				f = (it.cd79.maven.plugin.opencms.manifest.model.imports.File) jum.unmarshal(attrs);
			}
			if (acl.exists()) {
				JAXBContext jc = JAXBContext.newInstance(Accesscontrol.class);
				Unmarshaller jum = jc.createUnmarshaller();
				f.setAccesscontrol((Accesscontrol) jum.unmarshal(acl));
			}
			if (props.exists()) {
				JAXBContext jc = JAXBContext.newInstance(Properties.class);
				Unmarshaller jum = jc.createUnmarshaller();
				f.setProperties((Properties) jum.unmarshal(props));
			} else
				try {
					if (Configuration.getInstance().hasDefaultProperties(file.getName())) {
						JAXBContext jc = JAXBContext.newInstance(Properties.class);
						Unmarshaller jum = jc.createUnmarshaller();
						f.setProperties((Properties) jum.unmarshal(new ByteArrayInputStream(Configuration.getInstance()
								.getDefaultFilePropertiesFromExtension(file.getName()).getBytes())));
					}
				} catch (FileNotFoundException e1) {
					log.error(
							"shoudn't happen but if it is try to run again the building process or clean up the repository from it.cd79 - opencms-maven-plugin",
							e1);
				} catch (IOException e1) {
					log.error(
							"shoudn't happen but if it is try to run again the building process or clean up the repository from it.cd79 - opencms-maven-plugin",
							e1);
				} catch (NoFileTypeException e1) {
					log.error(
							"shoudn't happen but if it is try to run again the building process or clean up the repository from it.cd79 - opencms-maven-plugin",
							e1);
				}
			if (StringUtils.isBlank(f.getType())) {
				try {
					f.setType(p.getFileTypeFromExtension(file.getAbsolutePath()));
				} catch (NoFileTypeException e) {
					log.error("No Extension found", e);
				}
			}
			if (StringUtils.isBlank(f.getUsercreated())) {
				f.setUsercreated(p.get(Constants.PROPS_DEFAULT_EXPORT_FILES_FILE_USERCREATED));
			}
			if (StringUtils.isBlank(f.getUserlastmodified())) {
				f.setUserlastmodified(p.get(Constants.PROPS_DEFAULT_EXPORT_FILES_FILE_USERLASTMODIFIED));
			}
			if (StringUtils.isBlank(f.getFlags())) {
				f.setFlags(p.get(Constants.PROPS_DEFAULT_EXPORT_FILES_FILE_FLAGS));
			}
			if (StringUtils.isBlank(f.getSource()) && (StringUtils.isNotBlank(f.getType()) && !f.getType().equals(p.get(Constants.PROPS_FILE_TYPE_FOLDER)))) {
				f.setSource(relativeFilePath);
			}
			if (StringUtils.isBlank(f.getDestination())) {
				f.setDestination(relativeFilePath);
			}

			if (f.getAccesscontrol() == null) {
				f.setAccesscontrol(OF_IMPORT.createAccesscontrol());
			}
			if (f.getProperties() == null) {
				f.setProperties(OF_IMPORT.createProperties());
			}
			if (f.getDatecreated() == null) {
				f.setDatecreated(CmsDateUtil.getHeaderDate(now.getTime()));
			}
			if (f.getDatelastmodified() == null) {
				f.setDatelastmodified(CmsDateUtil.getHeaderDate(now.getTime()));
			}
			if (f.getRelations() == null) {
				f.setRelations(OF_IMPORT.createRelations());
			}
			// if (f.get == null)
			// f.set OF_IMPORT.create);
			files.getFile().add(f);
		}
		return files;
	}

	private Export fillInfoAndModule(Collection<File> moduleAttrs) throws FileNotFoundException, IOException, JAXBException {
		Export export = OF_IMPORT.createExport();
		Configuration p;
		p = Configuration.getInstance();
		for (File file : moduleAttrs) {
			if (file.getAbsolutePath().endsWith(p.get(Constants.PROPS_FILE_INFO_MODULE_ATTRIBUTES_FILE_EXTENSION))) {
				// Fill info part of the manifest.xml
				JAXBContext jc = JAXBContext.newInstance(Info.class);

				Unmarshaller jum = jc.createUnmarshaller();
				try {
					Info info = (Info) jum.unmarshal(file);
					export.setInfo(info);
				} catch (Exception e) {
					// log.warn("Maybe empty xml for " + p.get(Constants.PROPS_FILE_INFO_MODULE_ATTRIBUTES_FILE_EXTENSION));
					log.debug("Maybe empty xml for " + p.get(Constants.PROPS_FILE_INFO_MODULE_ATTRIBUTES_FILE_EXTENSION), e);
				}
			} else if (file.getAbsolutePath().endsWith(p.get(Constants.PROPS_FILE_MODULE_MODULE_ATTRIBUTES_FILE_EXTENSION))) {
				// Fill the export file for manifest.xml
				JAXBContext jc = JAXBContext.newInstance(Module.class);

				Unmarshaller jum = jc.createUnmarshaller();
				try {
					Module module = (Module) jum.unmarshal(file);
					export.setModule(module);
				} catch (Exception e) {
					// log.warn("Maybe empty xml for " + p.get(Constants.PROPS_FILE_MODULE_MODULE_ATTRIBUTES_FILE_EXTENSION));
					log.debug("Maybe empty xml for " + p.get(Constants.PROPS_FILE_MODULE_MODULE_ATTRIBUTES_FILE_EXTENSION), e);
				}
			} else if (file.getAbsolutePath().endsWith(p.get(Constants.PROPS_FILE_ACCOUNTS_MODULE_ATTRIBUTES_FILE_EXTENSION))) {
				// Fill the export file for manifest.xml
				JAXBContext jc = JAXBContext.newInstance(Accounts.class);

				Unmarshaller jum = jc.createUnmarshaller();
				try {
					Accounts accounts = (Accounts) jum.unmarshal(file);
					export.setAccounts(accounts);
				} catch (Exception e) {
					// log.warn("Maybe empty xml for " + p.get(Constants.PROPS_FILE_ACCOUNTS_MODULE_ATTRIBUTES_FILE_EXTENSION));
					log.debug("Maybe empty xml for " + p.get(Constants.PROPS_FILE_ACCOUNTS_MODULE_ATTRIBUTES_FILE_EXTENSION), e);
				}
			} else if (file.getAbsolutePath().endsWith(p.get(Constants.PROPS_FILE_PROJECT_MODULE_ATTRIBUTES_FILE_EXTENSION))) {
				// Fill the export file for manifest.xml
				JAXBContext jc = JAXBContext.newInstance(Projects.class);

				Unmarshaller jum = jc.createUnmarshaller();
				try {
					Projects projects = (Projects) jum.unmarshal(file);
					export.setProjects(projects);
				} catch (Exception e) {
					// log.warn("Maybe empty xml for " + p.get(Constants.PROPS_FILE_PROJECT_MODULE_ATTRIBUTES_FILE_EXTENSION));
					log.debug("Maybe empty xml for " + p.get(Constants.PROPS_FILE_PROJECT_MODULE_ATTRIBUTES_FILE_EXTENSION), e);
				}
			}
		}
		return export;
	}

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			Configuration.getInstance(properties);
		} catch (FileNotFoundException e1) {
			throw new MojoExecutionException("The file conf.properties may not exists", e1);
		} catch (IOException e1) {
			throw new MojoExecutionException("Error loading conf.properties", e1);
		}
		Object projectObject = getPluginContext().get("project");
		if ((projectObject != null) && (projectObject instanceof MavenProject)) {
			project = (MavenProject) projectObject;
		} else {
			throw new MojoExecutionException("Plugin not initialized");
		}
		try {
			File classes = null;
			if (StringUtils.isNotBlank(inputClassesDir)) {
				classes = new File(inputClassesDir);
			}
			File module = null;
			if (StringUtils.isNotBlank(inputOpencmsModuleDir)) {
				module = new File(inputOpencmsModuleDir);
			}
			File moduleAttributes = null;
			if (StringUtils.isNotBlank(inputOpencmsModuleAttributeDir)) {
				moduleAttributes = new File(inputOpencmsModuleAttributeDir);
			}
			File opencmsRoot = null;
			if (StringUtils.isNotBlank(inputOpencmsRootDir)) {
				opencmsRoot = new File(inputOpencmsRootDir);
			}
			File libsCopied = null;
			if (StringUtils.isNotBlank(inputLibsDir)) {
				libsCopied = new File(inputLibsDir);
			}
			File metaInf = null;
			if (StringUtils.isNotBlank(inputWebappMetaInfDir)) {
				metaInf = new File(inputWebappMetaInfDir);
			}
			generateManifest(classes, module, moduleAttributes, opencmsRoot, libsCopied, metaInf);
		} catch (FileNotFoundException e) {
			log.error("", e);
		} catch (IOException e) {
			log.error("", e);
		} catch (JAXBException e) {
			log.error("", e);
		}

	}
}
