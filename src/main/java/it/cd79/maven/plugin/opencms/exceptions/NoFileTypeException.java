package it.cd79.maven.plugin.opencms.exceptions;

public class NoFileTypeException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2289836584257806426L;

	public NoFileTypeException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoFileTypeException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoFileTypeException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NoFileTypeException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
