package it.cd79.maven.plugin.opencms.audit;

import java.util.HashMap;
import java.util.Map;

public class AuditCountersFactory {

	private static Map<String, AuditCounter> instance;

	public static AuditCounter getInstance(String counter) {
		if (instance == null)
			instance = new HashMap<String, AuditCounter>();
		if (!instance.containsKey(counter))
			instance.put(counter, new AuditCounter(counter));
		return instance.get(counter);
	}

	private AuditCountersFactory() {
		super();
	}

}
