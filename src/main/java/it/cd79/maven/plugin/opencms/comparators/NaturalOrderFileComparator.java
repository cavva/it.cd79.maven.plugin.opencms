package it.cd79.maven.plugin.opencms.comparators;

import it.cd79.maven.plugin.opencms.manifest.model.imports.File;

import java.util.Comparator;

public class NaturalOrderFileComparator implements Comparator<File> {
	// private static Log LOG = new SystemStreamLog();

	@Override
	public int compare(File o1, File o2) {
		return o1.getDestination().compareTo(o2.getDestination());
	}

}
