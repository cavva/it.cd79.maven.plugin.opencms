package it.cd79.maven.plugin.opencms.filenameFilters;

import it.cd79.maven.plugin.opencms.utils.CommonsFiltersUtils;
import it.cd79.maven.plugin.opencms.utils.Constants;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.IOFileFilter;

public class DirectoryFilter implements IOFileFilter {
	private static DirectoryFilter m_instance;

	private DirectoryFilter() {
	}

	public static DirectoryFilter getInstance() {
		if (m_instance == null) {
			m_instance = new DirectoryFilter();
		}
		return m_instance;
	}

	public boolean accept(File current, String name) {
		return CommonsFiltersUtils.checkDirectory(FilenameUtils.concat(current.getAbsolutePath(), name), Constants.PREFIX_DENIED_DIRECTORIES);
	}

	public boolean accept(File file) {
		return CommonsFiltersUtils.checkDirectory(file.getAbsolutePath(), Constants.PREFIX_DENIED_DIRECTORIES);
	}

}
