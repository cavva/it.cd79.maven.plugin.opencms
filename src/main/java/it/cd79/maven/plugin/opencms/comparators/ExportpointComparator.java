package it.cd79.maven.plugin.opencms.comparators;

import it.cd79.maven.plugin.opencms.manifest.model.modules.Exportpoint;

import java.util.Comparator;

public class ExportpointComparator implements Comparator<Exportpoint> {

	@Override
	public int compare(Exportpoint o1, Exportpoint o2) {
		return o1.getUri().compareTo(o2.getUri());
	}

}
