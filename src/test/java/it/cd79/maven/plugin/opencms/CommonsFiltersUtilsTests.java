package it.cd79.maven.plugin.opencms;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.cd79.maven.plugin.opencms.utils.CommonsFiltersUtils;

import java.net.URL;

import org.junit.Test;

public class CommonsFiltersUtilsTests {

	@Test
	public void testCheckFile() {
		URL testTxt = this.getClass().getResource("/config-files/test.txt");
		assertTrue(CommonsFiltersUtils.checkFile(testTxt.getFile(), "file.", "denied.file.", false));
		assertFalse(CommonsFiltersUtils.checkFile(testTxt.getFile(), "file.", "denied.file.", true));

		URL testTxtProps = this.getClass().getResource("/config-files/test.txt.opencms-props");
		URL testTxtAttrs = this.getClass().getResource("/config-files/test.txt.opencms-attrs");
		URL testTxtAcl = this.getClass().getResource("/config-files/test.txt.opencms-acl");

		assertTrue(CommonsFiltersUtils.checkFile(testTxtProps.getFile(), "file.module.attributes.", "denied.file.", false));
		assertFalse(CommonsFiltersUtils.checkFile(testTxtProps.getFile(), "file.module.attributes.", "denied.file.", true));
		assertTrue(CommonsFiltersUtils.checkFile(testTxtAttrs.getFile(), "file.module.attributes.", "denied.file.", false));
		assertFalse(CommonsFiltersUtils.checkFile(testTxtAttrs.getFile(), "file.module.attributes.", "denied.file.", true));
		assertTrue(CommonsFiltersUtils.checkFile(testTxtAcl.getFile(), "file.module.attributes.", "denied.file.", false));
		assertFalse(CommonsFiltersUtils.checkFile(testTxtAcl.getFile(), "file.module.attributes.", "denied.file.", true));

		assertTrue(CommonsFiltersUtils.checkFile(testTxtProps.getFile(), "file.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(testTxtProps.getFile(), "file.", "denied.file.", false));
		assertTrue(CommonsFiltersUtils.checkFile(testTxtAttrs.getFile(), "file.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(testTxtAttrs.getFile(), "file.", "denied.file.", false));
		assertTrue(CommonsFiltersUtils.checkFile(testTxtAcl.getFile(), "file.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(testTxtAcl.getFile(), "file.", "denied.file.", false));

		URL accounts = this.getClass().getResource("/config-files/accounts.opencms-module-attributes");
		URL info = this.getClass().getResource("/config-files/info.opencms-module-attributes");
		URL module = this.getClass().getResource("/config-files/module.opencms-module-attributes");
		URL project = this.getClass().getResource("/config-files/project.opencms-module-attributes");

		assertTrue(CommonsFiltersUtils.checkFile(accounts.getFile(), "file.module.attributes.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(accounts.getFile(), "file.module.attributes.", "denied.file.", false));
		assertTrue(CommonsFiltersUtils.checkFile(info.getFile(), "file.module.attributes.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(info.getFile(), "file.module.attributes.", "denied.file.", false));
		assertTrue(CommonsFiltersUtils.checkFile(module.getFile(), "file.module.attributes.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(module.getFile(), "file.module.attributes.", "denied.file.", false));
		assertTrue(CommonsFiltersUtils.checkFile(project.getFile(), "file.module.attributes.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(project.getFile(), "file.module.attributes.", "denied.file.", false));

		assertTrue(CommonsFiltersUtils.checkFile(accounts.getFile(), "file.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(accounts.getFile(), "file.", "denied.file.", false));
		assertTrue(CommonsFiltersUtils.checkFile(info.getFile(), "file.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(info.getFile(), "file.", "denied.file.", false));
		assertTrue(CommonsFiltersUtils.checkFile(module.getFile(), "file.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(module.getFile(), "file.", "denied.file.", false));
		assertTrue(CommonsFiltersUtils.checkFile(project.getFile(), "file.", "denied.file.", true));
		assertFalse(CommonsFiltersUtils.checkFile(project.getFile(), "file.", "denied.file.", false));

	}

}
