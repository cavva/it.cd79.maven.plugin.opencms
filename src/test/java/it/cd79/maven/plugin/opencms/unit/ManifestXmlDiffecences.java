package it.cd79.maven.plugin.opencms.unit;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceConstants;
import org.custommonkey.xmlunit.DifferenceListener;
import org.w3c.dom.Node;

public class ManifestXmlDiffecences implements DifferenceListener {

	private boolean	writeLog	= false;

	public void setWriteLog(boolean writeLog) {
		this.writeLog = writeLog;
	}

	public ManifestXmlDiffecences(boolean writeLog) {
		this.writeLog = writeLog;
	}

	@Override
	public int differenceFound(Difference difference) {

		switch (difference.getId()) {
			case DifferenceConstants.NAMESPACE_PREFIX_ID:
			case DifferenceConstants.NAMESPACE_URI_ID:
			case DifferenceConstants.SCHEMA_LOCATION_ID:
			case DifferenceConstants.HAS_CHILD_NODES_ID:
			case DifferenceConstants.CHILD_NODE_NOT_FOUND_ID:
			case DifferenceConstants.ATTR_NAME_NOT_FOUND_ID:
			case DifferenceConstants.ATTR_VALUE_EXPLICITLY_SPECIFIED_ID:
			case DifferenceConstants.ATTR_VALUE_ID:
			case DifferenceConstants.DOCTYPE_NAME_ID:
			case DifferenceConstants.NO_NAMESPACE_SCHEMA_LOCATION_ID:
			case DifferenceConstants.ATTR_SEQUENCE_ID:
			case DifferenceConstants.CDATA_VALUE_ID:
			case DifferenceConstants.COMMENT_VALUE_ID:
			case DifferenceConstants.DOCTYPE_PUBLIC_ID_ID:
			case DifferenceConstants.DOCTYPE_SYSTEM_ID_ID:
			case DifferenceConstants.ELEMENT_TAG_NAME_ID:
			case DifferenceConstants.ELEMENT_NUM_ATTRIBUTES_ID:
			case DifferenceConstants.PROCESSING_INSTRUCTION_TARGET_ID:
			case DifferenceConstants.PROCESSING_INSTRUCTION_DATA_ID:
			case DifferenceConstants.NODE_TYPE_ID:
			case DifferenceConstants.HAS_DOCTYPE_DECLARATION_ID:
				if (writeLog)
					writeLog("RETURN_ACCEPT_DIFFERENCE_1\t", difference);
				return RETURN_ACCEPT_DIFFERENCE;
			case DifferenceConstants.CHILD_NODELIST_SEQUENCE_ID:
			case DifferenceConstants.CHILD_NODELIST_LENGTH_ID:
				if (writeLog)
					writeLog("RETURN_IGNORE_DIFFERENCE_NODES_IDENTICAL", difference);
				return RETURN_IGNORE_DIFFERENCE_NODES_IDENTICAL;
			case DifferenceConstants.TEXT_VALUE_ID:
				if (difference.getControlNodeDetail().getNode().getParentNode().getNodeName().equalsIgnoreCase("createdate")
						|| difference.getControlNodeDetail().getNode().getParentNode().getNodeName().equalsIgnoreCase("datelastmodified")
						|| difference.getControlNodeDetail().getNode().getParentNode().getNodeName().equalsIgnoreCase("datecreated")) {
					if (writeLog)
						writeLog("RETURN_IGNORE_DIFFERENCE_NODES_SIMILAR", difference);
					return RETURN_IGNORE_DIFFERENCE_NODES_SIMILAR;
				}
				if (writeLog)
					writeLog("RETURN_ACCEPT_DIFFERENCE_2\t", difference);
				return RETURN_ACCEPT_DIFFERENCE;
		}
		if (writeLog)
			writeLog("RETURN_ACCEPT_DIFFERENCE_3\t", difference);
		return RETURN_ACCEPT_DIFFERENCE;
	}

	private void writeLog(String returnDifference, Difference difference) {
		try {
			String testDir = "target/manifest-tests";
			File d = new File(testDir);
			if (!d.exists())
				d.mkdirs();
			File f = new File(testDir + "/differences.txt");
			if (!f.exists())
				f.createNewFile();
			String content = FileUtils.readFileToString(f);
			FileUtils.writeStringToFile(f, content + "\n" + difference.getId() + "\t" + returnDifference + "\t" + difference);
		} catch (IOException e) {
		}
	}

	@Override
	public void skippedComparison(Node control, Node test) {
	}

}
