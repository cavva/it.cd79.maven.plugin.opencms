package it.cd79.maven.plugin.opencms.comparators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.cd79.maven.plugin.opencms.manifest.model.imports.Files;
import it.cd79.maven.plugin.opencms.test.Development;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.exceptions.ConfigurationException;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class FileComparatorTest {

	@Test
	public void testCompareJUnit() {
		FileComparator c = new FileComparator();
		int compare;
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f1 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f2 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f3 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f4 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		f1.setDestination("system/modules/it.cd79.www.templates");
		f1.setType("folder");
		f2.setDestination("system/modules/it.cd79.www.templates/templates");
		f2.setType("folder");
		f3.setDestination("system/modules/it.cd79.www.templates/schemas/service_page.xsd");
		f3.setType("plain");
		f4.setDestination("system/workplace/views/service_page.xsd");
		f4.setType("plain");

		// Folders in subdirs
		f1.setDestination("system/modules/it.cd79.www.templates");
		f2.setDestination("system/modules/it.cd79.www.templates/templates");
		compare = c.compare(f1, f2);
		assertTrue("F1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'\nExpected: " + FileComparator.OBJECT_1 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_1);
		compare = c.compare(f2, f1);
		assertTrue("F1: '" + f2.getDestination() + "'\nF2: '" + f1.getDestination() + "'\nExpected: " + FileComparator.OBJECT_2 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_2);

		// Folders not in subdirs
		f1.setDestination("system/modules/it.cd79.www.templates");
		f2.setDestination("system/workplace/views");
		compare = c.compare(f1, f2);
		assertTrue("F1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'\nExpected: NEGATIVE\nBut has: " + compare, compare < 0);
		compare = c.compare(f2, f1);
		assertTrue("F1: '" + f2.getDestination() + "'\nF2: '" + f1.getDestination() + "'\nExpected: POSITIVE\nBut has: " + compare, compare > 0);

		// File and folders in the same tree
		f1.setDestination("system/modules/it.cd79.www.templates");
		f3.setDestination("system/modules/it.cd79.www.templates/schemas/service_page.xsd");
		compare = c.compare(f1, f3);
		assertTrue("F1: '" + f1.getDestination() + "'\nF2: '" + f3.getDestination() + "'\nExpected: " + FileComparator.OBJECT_1 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_1);
		compare = c.compare(f3, f1);
		assertTrue("F1: '" + f3.getDestination() + "'\nF2: '" + f1.getDestination() + "'\nExpected: " + FileComparator.OBJECT_2 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_2);

		// Files in same dir
		f3.setDestination("system/modules/it.cd79.www.templates/page.xsd");
		f4.setDestination("system/modules/it.cd79.www.templates/service_page.xsd");
		compare = c.compare(f3, f4);
		assertTrue("F1: '" + f3.getDestination() + "'\nF2: '" + f4.getDestination() + "'\nExpected: NEGATIVE\nBut has: " + compare, compare < 0);
		compare = c.compare(f4, f3);
		assertTrue("F1: '" + f4.getDestination() + "'\nF2: '" + f3.getDestination() + "'\nExpected: POSITIVE\nBut has: " + compare, compare > 0);

		// Files in the same tree but one in subdir of the other
		f3.setDestination("system/modules/it.cd79.www.templates/page.xsd");
		f4.setDestination("system/modules/it.cd79.www.templates/schemas/service_page.xsd");
		compare = c.compare(f4, f3);
		assertTrue("F1: '" + f4.getDestination() + "'\nF2: '" + f3.getDestination() + "'\nExpected: " + FileComparator.OBJECT_2 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_2);
		compare = c.compare(f3, f4);
		assertTrue("F1: '" + f3.getDestination() + "'\nF2: '" + f4.getDestination() + "'\nExpected: " + FileComparator.OBJECT_1 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_1);

		// Files in the same tree but one in subdir of the other
		f3.setDestination("system/modules/it.cd79.www.templates/resources/img/header-en.png");
		f4.setDestination("system/modules/it.cd79.www.templates/resources/img/filetype/servicepage.gif");
		compare = c.compare(f4, f3);
		assertTrue("F1: '" + f4.getDestination() + "'\nF2: '" + f3.getDestination() + "'\nExpected: " + FileComparator.OBJECT_2 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_2);
		compare = c.compare(f3, f4);
		assertTrue("F1: '" + f3.getDestination() + "'\nF2: '" + f4.getDestination() + "'\nExpected: " + FileComparator.OBJECT_1 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_1);

		// Files in different tree
		f3.setDestination("system/modules/it.cd79.www.templates/page.xsd");
		f4.setDestination("system/workplace/it.cd79.www.templates/service_page.xsd");
		compare = c.compare(f3, f4);
		assertTrue("F1: '" + f3.getDestination() + "'\nF2: '" + f4.getDestination() + "'\nExpected: NEGATIVE\nBut has: " + compare, compare < 0);
		compare = c.compare(f4, f3);
		assertTrue("F1: '" + f4.getDestination() + "'\nF2: '" + f3.getDestination() + "'\nExpected: POSITIVE\nBut has: " + compare, compare > 0);

		// File and folders in the same tree
		f1.setDestination("system/modules/it.cd79.www.templates/resources/img/filetype");
		f3.setDestination("system/modules/it.cd79.www.templates/resources/img/arrow_right.png");
		compare = c.compare(f1, f3);
		assertTrue("F1: '" + f1.getDestination() + "'\nF2: '" + f3.getDestination() + "'\nExpected: " + FileComparator.OBJECT_2 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_2);
		compare = c.compare(f3, f1);
		assertTrue("F1: '" + f3.getDestination() + "'\nF2: '" + f1.getDestination() + "'\nExpected: " + FileComparator.OBJECT_1 + "\nBut has: " + compare,
				compare == FileComparator.OBJECT_1);

	}

	@Test
	public void testIsObject2IsSubfolderOfObject1() {
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f1 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f2 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		f1.setDestination("system/modules/it.cd79.opencms/test/");
		f1.setType("folder");
		f2.setType("folder");

		f2.setDestination("system/modules/it.cd79.opencms");
		assertFalse("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f1, f2));
		assertTrue("Those files have the same folder tree but inverted\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f2, f1));

		f2.setDestination("system/modules/it.cd79.opencms/test/test/test");
		assertTrue("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f1, f2));
		assertFalse("Those files have the same folder tree but inverted\nF1: '" + f2.getDestination() + "'\nF2: '" + f1.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f2, f1));

		f2.setDestination("sites/modules/it.cd79.opencms/test");

		assertFalse("Those files DO NOT have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f1, f2));
		assertFalse("Those files DO NOT have the same folder tree \nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f2, f1));

		f2.setDestination("system/worplace/views/");

		assertFalse("Those files DO NOT have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f1, f2));
		assertFalse("Those files DO NOT have the same folder tree \nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f2, f1));

		f1.setDestination("sites/default");
		f2.setDestination("sites/default/");
		assertFalse("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f1, f2));
		assertFalse("Those files have the same folder tree but inverted\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject2IsSubfolderOfObject1(f2, f1));
	}

	@Test
	public void testIsObject1FolderOfObject2() {
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f1 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f2 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		f1.setDestination("system/modules/it.cd79.opencms/test/");
		f1.setType("plain");
		f2.setType("plain");

		f2.setDestination("system/modules/it.cd79.opencms/test/testfile2.txt");
		assertTrue("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f1, f2));
		assertFalse("Those files have the same folder tree but inverted\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f2, f1));

		f2.setDestination("system/modules/it.cd79.opencms/test/test/test/testfile2.txt");
		assertTrue("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f1, f2));
		assertFalse("Those files have the same folder tree but inverted\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f2, f1));

		f2.setDestination("system/modules/it.cd79.opencms/testfile2.txt");

		assertFalse("Those files have the same folder tree but f1 is too deep\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f1, f2));
		assertFalse("Those files have the same folder tree but f1 is too deep and inverted\nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination()
				+ "'", FileComparator.isObject1FolderOfObject2(f2, f1));

		f2.setDestination("sites/modules/it.cd79.opencms/testfile2.txt");

		assertFalse("Those files DO NOT have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f1, f2));
		assertFalse("Those files DO NOT have the same folder tree \nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f2, f1));

		f2.setDestination("system/worplace/views/testfile2.txt");

		assertFalse("Those files DO NOT have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f1, f2));
		assertFalse("Those files DO NOT have the same folder tree \nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f2, f1));

		f1.setDestination("sites/default");
		f2.setDestination("sites/default/testfile2.txt");
		assertTrue("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f1, f2));
		assertFalse("Those files have the same folder tree but inverted\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.isObject1FolderOfObject2(f2, f1));
	}

	@Test
	public void testCompareParentFolders() {
		int compare;
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f1 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f2 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		f1.setDestination("system/modules/it.cd79.opencms/test/testfile.txt");
		f1.setType("plain");
		f2.setDestination("system/modules/it.cd79.opencms/test/testfile2.txt");
		f2.setType("plain");

		assertTrue("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.compareParentFolders(f1, f2) == 0);

		f2.setDestination("system/modules/it.cd79.opencms/testfile2.txt");

		assertTrue("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.compareParentFolders(f1, f2) == 0);
		assertTrue("Those files have the same folder tree \nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.compareParentFolders(f2, f1) == 0);

		f2.setDestination("sites/modules/it.cd79.opencms/testfile2.txt");

		compare = FileComparator.compareParentFolders(f2, f1);
		assertTrue("F1: '" + f2.getDestination() + "'\nF2: '" + f1.getDestination() + "'\nExpected: NEGATIVE\nBut has: " + compare, compare < 0);
		compare = FileComparator.compareParentFolders(f1, f2);
		assertTrue("F1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'\nExpected: POSITIVE\nBut has: " + compare, compare > 0);

		f2.setDestination("system/worplace/views/testfile2.txt");

		compare = FileComparator.compareParentFolders(f1, f2);
		assertTrue("F1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'\nExpected: NEGATIVE\nBut has: " + compare, compare < 0);
		compare = FileComparator.compareParentFolders(f2, f1);
		assertTrue("F1: '" + f2.getDestination() + "'\nF2: '" + f1.getDestination() + "'\nExpected: POSITIVE\nBut has: " + compare, compare > 0);

		f1.setDestination("testfile.txt");
		f2.setDestination("testfile2.txt");
		assertTrue("Those files have the same folder tree \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.compareParentFolders(f1, f2) == 0);

	}

	@Test
	public void testFilesHaveSameFolder() {
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f1 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f2 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		f1.setDestination("system/modules/it.cd79.opencms/test/testfile.txt");
		f1.setType("plain");
		f2.setDestination("system/modules/it.cd79.opencms/test/testfile2.txt");
		f2.setType("plain");

		assertTrue("Those files have the same folder \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.filesHaveSameFolder(f1, f2));

		f2.setDestination("system/modules/it.cd79.opencms/testfile2.txt");

		assertFalse("Those files DO NOT have the same folder \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.filesHaveSameFolder(f1, f2));
		assertFalse("Those files DO NOT have the same folder \nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.filesHaveSameFolder(f2, f1));

		f2.setDestination("sites/modules/it.cd79.opencms/testfile2.txt");

		assertFalse("Those files DO NOT have the same folder \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.filesHaveSameFolder(f1, f2));
		assertFalse("Those files DO NOT have the same folder \nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.filesHaveSameFolder(f2, f1));

		f1.setDestination("testfile.txt");
		f2.setDestination("testfile2.txt");
		assertTrue("Those files have the same folder \nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.filesHaveSameFolder(f1, f2));
	}

	@Test
	public void testParentFoldersAreFolderAndSubfolder() {
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f1 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		it.cd79.maven.plugin.opencms.manifest.model.imports.File f2 = new it.cd79.maven.plugin.opencms.manifest.model.imports.File();
		f1.setDestination("system/modules/it.cd79.opencms/test/testfile.txt");
		f1.setType("plain");
		f2.setType("plain");
		f2.setDestination("system/modules/it.cd79.opencms/test/test/testfile2.txt");

		assertTrue("Those files are in the same folder tree\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f1, f2));
		assertFalse("Those files are NOT in the same folder tree\nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f2, f1));

		f2.setDestination("system/modules/it.cd79.opencms/test/test/test/testfile2.txt");

		assertTrue("Those files are in the same folder tree\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f1, f2));
		assertFalse("Those files are NOT in the same folder tree\nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f2, f1));

		f2.setDestination("sites/modules/it.cd79.opencms/testfile2.txt");

		assertFalse("Those files are NOT in the same folder tree\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f1, f2));
		assertFalse("Those files are NOT in the same folder tree\nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f2, f1));

		f2.setDestination("system/modules/it.cd79.opencms/testfile2.txt");

		assertFalse("Those files are NOT in the same folder tree\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f1, f2));
		assertTrue("Those files are in the same folder tree\nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f2, f1));

		f2.setDestination("testfile2.txt");

		assertFalse("Those files are NOT in the same folder tree\nF1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f1, f2));
		assertTrue("Those files are in the same folder tree\nF2: '" + f1.getDestination() + "'\nF1: '" + f2.getDestination() + "'",
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f2, f1));

		// Files in the same tree but one in subdir of the other
		f1.setDestination("system/modules/it.cd79.www.templates/resources/img/header-en.png");
		f2.setDestination("system/modules/it.cd79.www.templates/resources/img/filetype/servicepage.gif");
		assertTrue(
				"F1: '" + f1.getDestination() + "'\nF2: '" + f2.getDestination() + "'\nExpected: TRUE\nBut has: "
						+ FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f1, f2),
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f1, f2));
		assertFalse(
				"F1: '" + f2.getDestination() + "'\nF2: '" + f1.getDestination() + "'\nExpected: FALSE\nBut has: "
						+ FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f2, f1),
				FileComparator.parentFolderObject2IsSubfolderOfParentFolderObject1(f2, f1));
	}

	@Category(Development.class)
	@Test
	public void testCompare() throws JAXBException, ConfigurationException, FileNotFoundException, SAXException, IOException {
		String targetDir = "target/comparators";
		String tosortFilePath = "src/test/resources/comparators/to-sort.xml";
		String sortedFilePath = "src/test/resources/comparators/sorted.xml";
		String generatedFilePath = targetDir + "/sorted_generated.xml";

		DetailedDiff detDiff = genericTest(targetDir, tosortFilePath, sortedFilePath, generatedFilePath);
		assertTrue("XSL transformation worked as expected " + detDiff, detDiff.similar());

	}

	@Category(Development.class)
	@Test
	public void testCompareComplexXML() throws JAXBException, ConfigurationException, FileNotFoundException, SAXException, IOException {
		String targetDir = "target/comparators";
		String tosortFilePath = "src/test/resources/comparators/to-sort-complex.xml";
		String sortedFilePath = "src/test/resources/comparators/sorted-complex.xml";
		String generatedFilePath = targetDir + "/sorted-complex_generated.xml";

		DetailedDiff detDiff = genericTest(targetDir, tosortFilePath, sortedFilePath, generatedFilePath);
		assertTrue("XSL transformation worked as expected " + detDiff, detDiff.similar());

	}

	private DetailedDiff genericTest(String targetDir, String tosortFilePath, String sortedFilePath, String generatedFilePath) throws JAXBException,
			IOException, PropertyException, SAXException, FileNotFoundException {
		JAXBContext jc = JAXBContext.newInstance(Files.class);
		Unmarshaller jum = jc.createUnmarshaller();
		Files filesToSort = (Files) jum.unmarshal(new File(tosortFilePath));

		Collections.sort(filesToSort.getFile(), new FileComparator());

		File expected = new File(sortedFilePath);

		File dir = new File(targetDir);
		if (!dir.exists())
			dir.mkdirs();

		File sorted = new File(generatedFilePath);
		if (!sorted.exists())
			sorted.createNewFile();

		Marshaller jm = jc.createMarshaller();
		jm.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jm.marshal(filesToSort, sorted);

		Document myGeneratedOutputXML = XMLUnit.buildDocument(XMLUnit.newControlParser(), new FileReader(sorted));
		Document myExpectedOutputXML = XMLUnit.buildDocument(XMLUnit.newControlParser(), new FileReader(expected));

		XMLUnit.setIgnoreWhitespace(Boolean.TRUE);
		Diff diff = new Diff(myGeneratedOutputXML, myExpectedOutputXML);
		DetailedDiff detDiff = new DetailedDiff(diff);
		return detDiff;
	}

}
