package it.cd79.maven.plugin.opencms;

import static org.junit.Assert.assertThat;
import it.cd79.maven.plugin.opencms.filenameFilters.DirectoryFilter;
import it.cd79.maven.plugin.opencms.filenameFilters.FileFilter;
import it.cd79.maven.plugin.opencms.filenameFilters.MainAttributesFilter;
import it.cd79.maven.plugin.opencms.test.Development;

import java.io.File;
import java.net.URL;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class FileUtilsTest {

	@Test
	public void testFindFilesAndDirsPostFiltered() {
		URL toTest = this.getClass().getResource("/to-test");
		File root = new File(toTest.getFile());
		File[] expected = {
				new File(FilenameUtils.concat(toTest.getFile(), "commons")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/pom.xml")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/.gitignore")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes/it")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes/it/cd79")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes/it/cd79/www")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes/it/cd79/www/commons")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes/it/cd79/www/commons/utils")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes/it/cd79/www/commons/utils/FrontEndUtils.class")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes/it/cd79/www/commons/utils/LocaleUtils.class")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/test")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/test/java")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/test/java/it")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/test/java/it/cd79")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/test/java/it/cd79/www")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/test/java/it/cd79/www/commons")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/test/java/it/cd79/www/commons/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/java")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/java/it")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/java/it/cd79")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/java/it/cd79/www")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/java/it/cd79/www/commons")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/java/it/cd79/www/commons/utils")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/java/it/cd79/www/commons/utils/LocaleUtils.java")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/java/it/cd79/www/commons/utils/FrontEndUtils.java")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module-attribute")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module-attribute/info.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module-attribute/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module-attribute/module.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module/tld")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module/tld/cd79.tld.opencms-attrs")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module/tld/cd79.tld")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/module/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/root")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/opencms/root/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/assemble")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/assemble/module-building.xml")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/template-files")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/template-files/-.opencms-attrs")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/template-files/-.opencms-acl")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/src/main/template-files/-.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target/opencms-pre-build")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/target/opencms-pre-build/manifest.xml")),
				new File(FilenameUtils.concat(toTest.getFile(), "commons/README.md")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/pom.xml")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/.gitignore")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target-for-test")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target-for-test/classes")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target-for-test/classes/it")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target-for-test/classes/it/cd79")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target-for-test/classes/it/cd79/www")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target-for-test/classes/it/cd79/www/templates")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target-for-test/classes/it/cd79/www/templates/workplace.properties")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/test")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/test/java")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/test/java/it")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/test/java/it/cd79")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/test/java/it/cd79/www")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/test/java/it/cd79/www/templates")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/test/java/it/cd79/www/templates/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/info.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/module.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/project.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/accounts.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/schemas")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/schemas/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/schemas/service_page.xsd")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/elements")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/elements/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/elements/commons")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/elements/commons/navigation.jsp")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/elements/commons/header.jsp")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/elements/commons/footer.jsp")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/home_16x16.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/arrow_right.png")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/x.svg")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/home_16x16.png")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/screwdriver.jpeg.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/pause.svg")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/arrow_right.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/lock_fill.svg")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/play.svg")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/header-it.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/unlock_fill.svg")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/header-en.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/screwdriver.jpeg")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/servicepage_big.png")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/servicepage_big.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/plain_big.png")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/plain_big.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/plain.gif")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/servicepage.gif.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/servicepage.gif")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/servicepage.png")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/plain.gif.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/servicepage.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/header-it.png")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/header-en.png")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/css")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/css/general.css")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/css/en.css")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/css/modal-interior.css")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/css/it.css")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/css/modal.css")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/css/general-service.css")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/js")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/js/modal.js")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/js/modal-interior.js")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/js/menu.js")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/templates")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/templates/landing_page.jsp")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/templates/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/templates/service_page.jsp")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/templates/main.jsp")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/formatters")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/formatters/service_page")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/formatters/service_page/service_page.jsp")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/formatters/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root/system")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root/system/workplace")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root/system/workplace/resources")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root/system/workplace/resources/filetypes")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root/system/workplace/resources/filetypes/servicepage_big.png")),
				new File(FilenameUtils.concat(toTest.getFile(),
						"templates/src/main/opencms/root/system/workplace/resources/filetypes/servicepage_big.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(),
						"templates/src/main/opencms/root/system/workplace/resources/filetypes/servicepage.gif.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root/system/workplace/resources/filetypes/servicepage.gif")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root/system/workplace/resources/filetypes/servicepage.png")),
				new File(FilenameUtils.concat(toTest.getFile(),
						"templates/src/main/opencms/root/system/workplace/resources/filetypes/servicepage.png.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/root/.gitkeep")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/assemble")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/assemble/module-building.xml")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/resources")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/resources/it")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/resources/it/cd79")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/resources/it/cd79/www")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/resources/it/cd79/www/templates")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/resources/it/cd79/www/templates/workplace.properties")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/template-files")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/template-files/-.opencms-attrs")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/template-files/-.opencms-acl")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/src/main/template-files/-.opencms-props")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target/opencms-pre-build")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/target/opencms-pre-build/manifest.xml")),
				new File(FilenameUtils.concat(toTest.getFile(), "templates/README.md")) };
		Collection<File> generated = it.cd79.maven.plugin.opencms.utils.FileUtils.findFilesAndDirsPostFiltered(root, TrueFileFilter.INSTANCE);
		assertThat(generated, IsIterableContainingInAnyOrder.containsInAnyOrder(expected));
	}

	@Category(Development.class)
	@Test
	public void testFindFilesAndDirsPostFilteredWithDirectoryFilter() {
		URL toTest = this.getClass().getResource("/to-test");
		File root = new File(toTest.getFile());
		File[] expected = { new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/formatters")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/elements")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/templates")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/css")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/images")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/js")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/schemas")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/classes")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/classes/it")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/classes/it/cd79")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/classes/it/cd79/opencms")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/classes/it/cd79/opencms/maven")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/classes/it/cd79/opencms/maven/test")) };
		Collection<File> generated = it.cd79.maven.plugin.opencms.utils.FileUtils.findFilesAndDirsPostFiltered(root, DirectoryFilter.getInstance());
		assertThat(generated, IsIterableContainingInAnyOrder.containsInAnyOrder(expected));
	}

	@Category(Development.class)
	@Test
	public void testFindFilesAndDirsPostFilteredWithFileFilter() {
		URL toTest = this.getClass().getResource("/to-test");
		File root = new File(toTest.getFile());
		File[] expected = {
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/templates/test.jsp")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/css/test.css")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/images/test.png")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/images/test.jpg")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/images/test.svg")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/images/test.gif")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/resources/js/test.js")),
				new File(FilenameUtils.concat(toTest.getFile(), "system/modules/it.cd79.opencms.maven.test/schemas/test.xsd")),
				new File(FilenameUtils.concat(toTest.getFile(),
						"system/modules/it.cd79.opencms.maven.test/classes/it/cd79/opencms/maven/test/workplace.properties")),
				new File(FilenameUtils.concat(toTest.getFile(),
						"system/modules/it.cd79.opencms.maven.test/classes/it/cd79/opencms/maven/test/ModuleAction.class")) };
		Collection<File> generated = it.cd79.maven.plugin.opencms.utils.FileUtils.findFilesAndDirsPostFiltered(root, FileFilter.getInstance());
		assertThat(generated, IsIterableContainingInAnyOrder.containsInAnyOrder(expected));
	}

	@Category(Development.class)
	@Test
	public void testFindFilesAndDirsPostFilteredWithMainAttributesFilter() {
		URL toTest = this.getClass().getResource("/to-test");
		File root = new File(toTest.getFile());
		File[] expected = { new File(FilenameUtils.concat(toTest.getFile(), "")),
				new File(FilenameUtils.concat(toTest.getFile(), "accounts.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "module.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "project.opencms-module-attributes")),
				new File(FilenameUtils.concat(toTest.getFile(), "info.opencms-module-attributes")) };
		Collection<File> generated = it.cd79.maven.plugin.opencms.utils.FileUtils.findFilesAndDirsPostFiltered(root, MainAttributesFilter.getInstance());
		generated.add(root);
		assertThat(generated, IsIterableContainingInAnyOrder.containsInAnyOrder(expected));
	}

	/**
	 * run it if you changed to-test subdir of test. It generate a full list of files, which must be filtered for all test. For
	 * {@link #testFindFilesAndDirsPostFiltered()} just remove the first element.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		URL toTest = FileUtilsTest.class.getResource("/to-test");
		File root = new File(toTest.getFile());
		Collection<File> expected = FileUtils.listFilesAndDirs(root, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		boolean first = true;
		System.out.print("File[] expected = {");
		for (File file : expected) {
			int offset;
			if (first) {
				offset = 0;
				first = false;
			} else {
				offset = 1;
				System.out.println(",");
			}
			System.out.print("new File(FilenameUtils.concat(toTest.getFile(), \"" + file.getAbsolutePath().substring(root.getAbsolutePath().length() + offset)
					+ "\"))");
		}
		System.out.println("};");
	}
}
