package it.cd79.maven.plugin.opencms.stub;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Developer;
import org.apache.maven.plugin.testing.stubs.MavenProjectStub;

public class ManifestMavenProjectStub extends MavenProjectStub {

	private List<Developer>		developers;

	private List<Dependency>	dependencies;

	@Override
	public List<Dependency> getDependencies() {
		if (dependencies != null)
			return dependencies;
		else
			return Collections.<Dependency> emptyList();
	}

	@Override
	public void setDependencies(List<Dependency> dependencies) {
		this.dependencies = dependencies;
	}

	public ManifestMavenProjectStub(File pom, String basedir) {
		super();
		this.readModel(pom);

		this.setArtifactId(this.getModel().getArtifactId());
		this.setGroupId(this.getModel().getGroupId());
		this.setName(this.getModel().getName());
		this.addDeveloper(this.getModel().getDevelopers().get(0));
		this.setVersion(this.getModel().getVersion());
		this.addCompileSourceRoot(basedir + "target/classes");
		super.setModel(this.getModel());
		this.setDependencies(this.getModel().getDependencies());

	}

	@Override
	public void setDevelopers(List<Developer> list) {
		developers = list;
	}

	@Override
	public List<Developer> getDevelopers() {
		return developers;
	}

	@Override
	public void addDeveloper(Developer developer) {
		if (developers == null)
			developers = new ArrayList<Developer>();
		developers.add(developer);
	}

}
