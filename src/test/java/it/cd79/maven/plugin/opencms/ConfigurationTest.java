package it.cd79.maven.plugin.opencms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.cd79.maven.plugin.opencms.exceptions.NoFileTypeException;
import it.cd79.maven.plugin.opencms.utils.Configuration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

//@Category(Development.class)
public class ConfigurationTest {

	@Test
	public void testGetFileTypeFromExtension() throws FileNotFoundException, NoFileTypeException, IOException {
		URL toTest = this.getClass().getResource("/to-test");
		List<String[]> listExpected = new ArrayList<String[]>();
		listExpected.add(new String[] { "folder", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/templates") });

		listExpected.add(new String[] { "jsp", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/templates/landing_page.jsp") });
		listExpected
				.add(new String[] { "plain", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/css/general-service.css") });
		listExpected.add(new String[] { "image", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/lock_fill.svg") });
		listExpected.add(new String[] { "image", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/screwdriver.jpeg") });
		listExpected.add(new String[] { "image", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/header-it.png") });
		listExpected
				.add(new String[] { "image", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/plain.gif") });
		listExpected
				.add(new String[] { "image", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/img/filetype/plain.gif") });
		listExpected.add(new String[] { "plain", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/resources/js/modal-interior.js") });
		listExpected.add(new String[] { "plain", FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module/schemas/service_page.xsd") });
		listExpected.add(new String[] { "plain",
				FilenameUtils.concat(toTest.getFile(), "templates/target-for-test/classes/it/cd79/www/templates/workplace.properties") });
		listExpected.add(new String[] { "binary",
				FilenameUtils.concat(toTest.getFile(), "commons/target-for-test/classes/it/cd79/www/commons/utils/FrontEndUtils.class") });
		listExpected.add(new String[] { "error",
				FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/accounts.opencms-module-attributes") });
		listExpected.add(new String[] { "error",
				FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/module.opencms-module-attributes") });
		listExpected.add(new String[] { "error",
				FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/project.opencms-module-attributes") });
		listExpected.add(new String[] { "error",
				FilenameUtils.concat(toTest.getFile(), "templates/src/main/opencms/module-attribute/info.opencms-module-attributes") });
		for (String[] key : listExpected) {
			if (key[0].equalsIgnoreCase("error"))
				try {
					if (StringUtils.isNotBlank(Configuration.getInstance().getFileTypeFromExtension(key[1])))
						assertTrue("Should be an exception", false);
					assertTrue("Should be an exception and not just a null or empty string", false);
				} catch (Throwable e) {
					assertTrue(true);
				}
			else
				assertEquals(key[0], Configuration.getInstance().getFileTypeFromExtension(key[1]));
		}
	}

	// @Test
	public void testGetFileGroupsElements() {

	}

}
