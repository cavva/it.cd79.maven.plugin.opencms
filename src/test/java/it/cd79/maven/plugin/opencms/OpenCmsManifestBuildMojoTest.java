package it.cd79.maven.plugin.opencms;

import it.cd79.maven.plugin.opencms.stub.ManifestMavenProjectStub;
import it.cd79.maven.plugin.opencms.unit.ManifestComparisonController;
import it.cd79.maven.plugin.opencms.unit.ManifestXmlDiffecences;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.MavenProject;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceEngine;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.examples.RecursiveElementNameAndTextQualifier;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

//@Category(Development.class)
public class OpenCmsManifestBuildMojoTest extends AbstractMojoTestCase {

	private static final String	SRC_TEST_RESOURCES			= "src/test/resources/";
	private static final String	TEMPLATES					= "templates/";
	private static final String	COMMONS						= "commons/";
	private static final String	SRC_TEST_RESOURCES_TO_TEST	= SRC_TEST_RESOURCES + "to-test/";

	private static Log			LOG							= new SystemStreamLog();

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		// required for mojo lookups to work
		super.setUp();
	}

	public void testManifestXmlTemplates() throws Exception {
		String mainDir = SRC_TEST_RESOURCES_TO_TEST + TEMPLATES;
		String expectedManifest = "results/templates-manifest.xml";
		String generatedManifest = "target/opencms-pre-build/manifest.xml";

		genericTest(mainDir, expectedManifest, generatedManifest);

	}

	public void testManifestXmlCommons() throws Exception {
		String mainDir = SRC_TEST_RESOURCES_TO_TEST + COMMONS;
		String expectedManifest = "results/commons-manifest.xml";
		String generatedManifest = "target/opencms-pre-build/manifest.xml";

		genericTest(mainDir, expectedManifest, generatedManifest);

	}

	private void genericTest(String mainDir, String expectedManifest, String generatedManifest) throws Exception, MojoExecutionException, MojoFailureException,
			SAXException, IOException, FileNotFoundException {
		File pom = getTestFile(mainDir + "pom.xml");
		assertNotNull(pom);
		assertTrue(pom.exists());

		OpenCmsManifestBuildMojo ocmbm = (OpenCmsManifestBuildMojo) lookupMojo("manifest", pom);

		HashMap<Object, Object> pluginContext = new HashMap<Object, Object>();
		pluginContext.put("project", new ManifestMavenProjectStub(pom, mainDir));
		ocmbm.setPluginContext(pluginContext);

		ocmbm.outputDir = mainDir + ocmbm.outputDir;
		ocmbm.inputClassesDir = mainDir + ocmbm.inputClassesDir;
		ocmbm.inputLibsDir = mainDir + ocmbm.inputLibsDir;
		ocmbm.inputOpencmsModuleAttributeDir = mainDir + ocmbm.inputOpencmsModuleAttributeDir;
		ocmbm.inputOpencmsModuleDir = mainDir + ocmbm.inputOpencmsModuleDir;
		ocmbm.inputOpencmsRootDir = mainDir + ocmbm.inputOpencmsRootDir;

		assertNotNull(ocmbm);
		ocmbm.project = (MavenProject) pluginContext.get("project");

		ocmbm.execute();

		File expected = new File(FilenameUtils.concat(getBasedir(), SRC_TEST_RESOURCES + expectedManifest));
		File generated = new File(FilenameUtils.concat(getBasedir(), mainDir + generatedManifest));

		Document myGeneratedOutputXML = XMLUnit.buildDocument(XMLUnit.newControlParser(), new FileReader(generated));
		Document myExpectedOutputXML = XMLUnit.buildDocument(XMLUnit.newControlParser(), new FileReader(expected));

		XMLUnit.setIgnoreWhitespace(Boolean.TRUE);
		XMLUnit.setIgnoreComments(true);
		DifferenceEngine engine = new DifferenceEngine(ManifestComparisonController.getInstance());
		ManifestXmlDiffecences listener = new ManifestXmlDiffecences(true);
		RecursiveElementNameAndTextQualifier elementQuelifier = new RecursiveElementNameAndTextQualifier();
		engine.compare(myGeneratedOutputXML, myExpectedOutputXML, listener, elementQuelifier);

		Diff myDiff = new Diff(myGeneratedOutputXML, myExpectedOutputXML, engine, elementQuelifier);
		DetailedDiff detDiff = new DetailedDiff(myDiff);

		// Suppressed because necessary
		@SuppressWarnings("unchecked")
		List<Difference> diffs = detDiff.getAllDifferences();

		listener.setWriteLog(false);
		boolean different = false;
		for (Difference d : diffs) {
			if (listener.differenceFound(d) == DifferenceListener.RETURN_ACCEPT_DIFFERENCE) {
				different = true;
				LOG.error("Difference : " + d.toString());
			} else {
				LOG.info("Similar : " + d.toString());
			}
		}
		assertTrue("XSL transformation worked as expected TRUE but was " + (!different), !different);
	}

}
