package it.cd79.maven.plugin.opencms.unit;

import org.custommonkey.xmlunit.ComparisonController;
import org.custommonkey.xmlunit.Difference;

public class ManifestComparisonController implements ComparisonController {
	// private static Log LOG = new SystemStreamLog();

	private static ManifestComparisonController	INSTANCE;

	private ManifestComparisonController() {
		super();
	}

	public static ManifestComparisonController getInstance() {
		if (INSTANCE == null)
			INSTANCE = new ManifestComparisonController();
		return INSTANCE;
	}

	@Override
	public boolean haltComparison(Difference afterDifference) {
		return false;
	}

}
