var Menu = function() {
	this.navbar = jQuery("nav");
	this.submenus = new Array();
	if (typeof this.navbar == 'undefined' || this.navbar === null)
		this.navbar = jQuery(".navbar");
	var children = this.navbar.children("ul").children("li");
	children.each(jQuery.proxy(function(id, obj) {
		var ul = jQuery(obj).children("ul");
		if (ul.length > 0) {
			var ulId = "ul_" + Math.floor(Math.random() * 1000000);
			ul.attr("id", ulId);
			this.submenus[ulId] == null;
			jQuery(obj).mouseenter(jQuery.proxy(function() {
				this.stop(true, true).slideDown('fast');
			}, ul)).mouseleave(jQuery.proxy(function() {
				this.stop(true, true).slideUp('fast');
			}, ul));
		}
	}, this));
};

/**
 * 
 * @param barId
 *            Bar id, it could be written either with "#" or without, the code
 *            will identify the bar anyway.
 * @param height
 *            the Height if negative will be the css "top" else positive one
 *            will be the height the bar will be out of the top.
 * @param border
 *            identifies the border where the bar is attached, you can use either the word or the id (top = 0, right = 1, bottom = 2, left = 3)
 * @param hiding
 *            is the boolean that says if it is in stop or selfhidding mode.
 * @param pausing
 *            is the boolean that identify the auto-showing mode is on or temporary off. Similar to the stop mode but in the hiding configuation.
 */
var HideBar = function(barId, height, border, hiding, pausing) {
	this.f_getBorder = function(border) {
		switch (border) {
		case 0:
			return "top";
		case 1:
			return "right";
		case 2:
			return "bottom";
		case 3:
			return "left";
		default:
			return border;
		}
	};

	this.f_getBorderInt = function(border) {
		if (border == "top") {
			return 0;
		}
		if (border == "right") {
			return 1;
		}
		if (border == "bottom") {
			return 2;
		}
		if (border == "left") {
			return 3;
		}
		return border;
	};

	this.f_boxDimension = function(){
		return (this.v_borderInt % 2 == 1) ? this.v_bar.outerWidth() : this.v_bar.outerHeight();
	};
	
	this.f_getHeight = function(height) {
		if (height > 0)
			return height - this.f_boxDimension();
		else
			return height;
	};

	this.f_stopHideBar = function(force) {
		this.f_hide(jQuery.proxy(function(){
			this.f_change(true);
			this.f_showFixed();
		},this), force);
	};

	this.f_startHideBar = function(force) {
		this.f_hideFixed(jQuery.proxy(function(){
			this.f_change(false);
			this.f_show(this.v_height, force);
		},this));
	};

	this.f_hide = function(callback, force) {
		if ((this.v_stop || (!this.v_stop && this.v_pause)) && !force)
			return;
		this.v_bar.stop().animate(this.f_getBorderWithDimension(-(10+this.f_boxDimension())), callback);
	};

	this.f_show = function(dimension, force) {
		if ((this.v_stop || (!this.v_stop && this.v_pause)) && !force)
			return;
		this.v_bar.stop().animate(this.f_getBorderWithDimension(dimension));
	};

	this.f_hideFixed = function(callback) {
		this.v_bar.fadeOut("slow",jQuery.proxy(function() {
			this.v_bar.css(this.f_getBorderWithDimension(this.f_boxDimension()));
			callback();
		}, this));
	};

	this.f_showFixed = function() {
		if (this.v_bar.is(":visible"))
			this.v_bar.css("display", "none");
		this.v_bar.css(this.f_getBorderWithDimension(0));
		this.v_bar.fadeIn("slow");
	};

	this.f_change = function(fixed) {
		if (fixed) {
			this.v_stop = true;
			this.v_bar.css("display", "none");
			this.v_bar.css("position","relative");
		} else {
			this.v_stop = false;
			this.v_bar.css(this.f_getBorderWithDimension(-(10+this.f_boxDimension())));
			this.v_bar.css("position","fixed");
			this.v_bar.css("display", "block");
		}
	};

	this.f_getBorderWithDimension = function(dimension) {
		var res = new Array();
		res[this.v_border] = (dimension == 0 || (typeof dimension == 'string' && dimension.indexOf("px") > -1)) ? dimension
				: dimension + "px";
		return res;
	};

	var id = (barId.indexOf("#") == 0 ? barId : "#" + barId);
	this.v_bar = jQuery(id);
	this.v_stop = !hiding;
	this.v_pause = pausing;
	this.v_border = this.f_getBorder(border);
	this.v_borderInt = this.f_getBorderInt(border);
	this.v_height = this.f_getHeight(height);

	this.v_bar.mouseenter(jQuery.proxy(function() {
		this.f_show(0, false);
	}, this)).mouseleave(jQuery.proxy(function() {
		this.f_show(this.v_height, false);
	}, this));
};

HideBar.prototype.toggleHideBar = function() {
	if (this.v_stop)
		this.f_startHideBar(true);
	else
		this.f_stopHideBar(true);
	return false;
};

HideBar.prototype.getStop = function() {
	return this.v_stop;
};

HideBar.prototype.getPause = function() {
	return this.v_pause;
};

HideBar.prototype.stopMoving = function() {
	this.f_stopHideBar(true);
};

HideBar.prototype.startMoving = function() {
	this.f_startHideBar(true);
};

HideBar.prototype.togglePause = function() {
	this.v_pause = !this.v_pause;
};
