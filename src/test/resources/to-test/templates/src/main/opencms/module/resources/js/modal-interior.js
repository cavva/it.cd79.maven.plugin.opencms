var HiddenLink = function(linkPrefixArray, textArrayLink, textArray) {
	this.linkPrefixArray = linkPrefixArray;
	this.textArrayLink = textArrayLink;
	this.textArray = textArray; // Optional

};

HiddenLink.prototype.getLink = function() {
	var link = jQuery("<a class=\"hiddenLink\">");
	var useLinkForText = false;
	if (typeof this.textArray == 'undefined' || this.textArray === null) {
		useLinkForText = true;
	}
	var href = "";
	var text = "";
	for ( var i = 0; i < this.linkPrefixArray.length; i++) {
		href += String.fromCharCode(this.linkPrefixArray[i]);
	}
	if (useLinkForText) {
		for ( var i = 0; i < this.textArrayLink.length; i++) {
			href += String.fromCharCode(this.textArrayLink[i]);
			text += String.fromCharCode(this.textArrayLink[i]);
		}
	} else {
		for ( var i = 0; i < this.textArrayLink.length; i++) {
			href += String.fromCharCode(this.textArrayLink[i]);
		}
		for ( var i = 0; i < this.textArray.length; i++) {
			text += String.fromCharCode(this.textArray[i]);
		}
	}
	link.append(text);
	link.attr("href", href);
	return link;
};

