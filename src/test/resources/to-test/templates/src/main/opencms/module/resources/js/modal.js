var Construction = function(id, innerHtml, settings, closeIcon) {
	this.overlayId = "overlay_" + Math.floor(Math.random() * 1000000);
	this.modalId = "modal_" + Math.floor(Math.random() * 1000000);
	this.closeButtonId = "close_" + Math.floor(Math.random() * 1000000);
	this.innerHtml = innerHtml;
	this.button;
	this.settings = settings;
	if (id.indexOf("#") != 0)
		this.button = jQuery("#" + id);
	else
		this.button = jQuery(id);
	this.button.css("cursor", "pointer");
	this.button.attr("href", "javascript:void(0);");
	this.closeIcon = closeIcon;
	this.button.click(jQuery.proxy(function(event) {
		this.openOverlay(event);
	}, this));
};

Construction.prototype.openOverlay = function(event) {
	var overlay = jQuery("<div id=\"" + this.overlayId
			+ "\" class=\"overlay\">");
	
	overlay.css({
		"height": Math.max(jQuery(document).height(), jQuery(window).height()),
		"width" : Math.max(jQuery(document).width(), jQuery(window).width())
	});
	
	overlay.click(jQuery.proxy(function() {
		this.closeOverlay();
	}, this));

	var modal = jQuery("<div id=\"" + this.modalId + "\" class=\"modal\">");
	if (typeof this.settings != 'undefined' && this.settings !== null) {
		modal.css({
			"width" : (this.settings.width || "auto"),
			"height" : (this.settings.height || "auto")
		});
	}

	modal.append(this.innerHtml);

	modal.hide();
	jQuery("body").append(modal);
	var hm = modal.outerHeight();
	var wm = modal.outerWidth();
	modal.css("top", jQuery(window).scrollTop()
			+ (Math.max(jQuery(window).height() - hm, 0) / 2));
	modal.css("left", jQuery(window).scrollLeft()
			+ (Math.max(jQuery(window).width() - wm, 0) / 2));

	var closeButton = jQuery("<div id=\"" + this.closeButtonId
			+ "\" class=\"button\">");

	var img = jQuery("<img src=\"" + this.closeIcon +"\" class=\"buttonImage\">");
	closeButton.append(img);

	modal.append(closeButton);

	var h = closeButton.children().height();
	var w = closeButton.children().width();
	var top = h / 2;
	var right = w / 2;
	closeButton.css({
		"top" : "-" + top + "px",
		"right" : "-" + right + "px"
	});

	closeButton.click(jQuery.proxy(function() {
		this.closeOverlay();
	}, this));

	jQuery("body").append(overlay);
	overlay.fadeIn('slow', function() {
		modal.fadeIn('slow');
		closeButton.fadeIn('slow');
	});

};

Construction.prototype.closeOverlay = function() {
	var overlay = jQuery("#" + this.overlayId);
	var modal = jQuery("#" + this.modalId);
	modal.fadeOut('slow', function() {
		overlay.fadeOut('slow', function() {
			overlay.remove();
			modal.remove();
		});
	});
};
